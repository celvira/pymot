import torch
import numpy as np

from pymot.moments import estimate_moments
from pymot.quantile import get_func_quantile

import matplotlib.pyplot as plt

num_samples = 100
num_moments = 25
dtype = torch.float64

	# Mixture of 2 Gaussians
vec_X = np.zeros(num_samples)
vec_X[:50] = -1. + .25*np.random.randn(50)
vec_X[50:] =  1. + .25*np.random.randn(50)
#vec_X[500:] =  3*np.random.rand(500)

# 1. Compute numpy (~true) quantile function
vecq = np.linspace(0, 1, 100)
vec_quantiles = np.quantile(vec_X, vecq)

# 2. Compute pymot quantile function
moments = estimate_moments(
	torch.tensor(vec_X, dtype=torch.float64).view(1, num_samples), num_moments, 'om', dtype=dtype)

qfunc = get_func_quantile(moments, dtype)
qfunc_samples = get_func_quantile(torch.tensor(vec_X, dtype=torch.float64), dtype)

f, ax = plt.subplots(1, 1, figsize=(8, 4.4))
ax.plot(vecq, vec_quantiles)
ax.plot(vecq, np.array([qfunc(q) for q in vecq]))
ax.plot(vecq, np.array([qfunc_samples(q) for q in vecq]))

plt.legend(['True qfunc', 'estimated qfunc', 'one from samples'])

plt.show()