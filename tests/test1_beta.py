#	##########################################################
#
#					Test 1
#					------
#
# Recover pdf, cdf and quantile function for the beta density.
# In that case, the moments are available in close form
#
#	##########################################################

import torch

from pymot.om.cdf import get_cdf
from pymot.quantile import get_func_quantile
from pymot.om.moments import beta_moments

from scipy.stats import beta
import matplotlib.pyplot as plt


k = 33
a = 1.
b = 5.
dtype=torch.float64

beta_moments = beta_moments(k, a, b)


f, ax = plt.subplots(3, 1, figsize=(16, 9))
ranget = torch.linspace(0, 1, 201).numpy()

# ## pdf test
# pdf_hat = estimators.pdf(beta_true_moments)
# ax[0].plot(ranget, np.array([pdf_hat(t) for t in ranget]))
# ax[0].plot(ranget, np.array([beta.pdf(t, a, b) for t in ranget]))
# ax[0].set_title('pdf', fontsize=16)

## cdf test
cdf_hat = get_cdf(beta_moments, dtype=dtype)
ax[1].plot(ranget, torch.tensor([cdf_hat(t) for t in ranget]).numpy())
ax[1].plot(ranget, torch.tensor([beta.cdf(t, a, b) for t in ranget]).numpy())
ax[1].set_title('cdf', fontsize=16)

## quantile test
quantile_hat = get_func_quantile(beta_moments, dtype=dtype)
ax[2].plot(ranget, torch.tensor([quantile_hat(t) for t in ranget]).numpy())
ax[2].plot(ranget, torch.tensor([beta.ppf(t, a, b) for t in ranget]).numpy())
ax[2].set_title('quantile function', fontsize=16)
plt.show()
