import torch
import numpy as np

from source.loss import _compute_quantile_func_from_moment

import matplotlib.pyplot as plt

gen = torch.distributions.beta.Beta(torch.tensor([1.]), torch.tensor([1.]))


num_samples = 256
num_slices = 2

samples = torch.zeros(num_samples, num_slices).double().to("cpu")
for l in range(num_slices):
	samples[:, l] = gen.sample([num_samples]).double()[:, 0]
# samples = gen.rsample([num_samples]).double()

samples[:, 0] = 2 * samples[:, 0]


print(samples.size())

tensor_quantile = _compute_quantile_func_from_moment(samples, 
   num_slices, 25, [0, 1], [2, 1], "cpu")

print(tensor_quantile.size())

f, ax = plt.subplots(1, 1)

ax.plot(np.linspace(0, 1, 26), tensor_quantile.detach().numpy()[0, :])
ax.plot(np.linspace(0, 1, 26), tensor_quantile.detach().numpy()[1, :])

plt.show()