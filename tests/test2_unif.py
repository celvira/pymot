#	##########################################################
#
#					Test 2
#					------
#
# Recover pdf, cdf and quantile function for the uniform 
# distribution on [0, T]
# In that case, the moments are available in close form
#
#	##########################################################

import torch

from pymot.om.cdf import get_cdf
from pymot.quantile import get_func_quantile
from pymot.om.moments import unif_moments

from scipy.stats import beta
import matplotlib.pyplot as plt

k = 27
T = 5.
dtype = torch.float64

unifs_moments = unif_moments(k, T, dtype=dtype)


f, ax = plt.subplots(3, 1, figsize=(16, 9))
ranget = torch.linspace(0, T, 201).numpy()
rangeq = torch.linspace(0, 1, 201).numpy()

## pdf test
# pdf_hat = estimators.pdf(beta_true_moments, T=T)
# ax[0].plot(ranget, np.array([pdf_hat(t) for t in ranget]))
# ax[0].plot(ranget, np.array([1. / T for t in ranget]))
# ax[0].set_title('pdf', fontsize=16)

## cdf test
cdf_hat = get_cdf(unifs_moments)
ax[1].plot(ranget, torch.tensor([cdf_hat(t) for t in ranget]).numpy())
ax[1].plot(ranget, torch.tensor([t / T for t in ranget]).numpy())
ax[1].set_title('cdf', fontsize=16)

## quantile test
quantile_hat = get_func_quantile(unifs_moments)
ax[2].plot(rangeq, torch.tensor([quantile_hat(q) for q in rangeq]).numpy())
ax[2].plot(rangeq, torch.tensor([q * T for q in rangeq]).numpy())
ax[2].set_title('quantile function', fontsize=16)
plt.show()
