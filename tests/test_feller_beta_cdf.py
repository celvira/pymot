#	##########################################################
#
#					Test 1
#					------
#
# Recover pdf, cdf and quantile function for the beta density.
# In that case, the moments are available in close form
#
#	##########################################################

import torch
import numpy as np
from scipy.stats import beta

from pymot.moments import estimate_moments
from pymot.quantile import get_func_quantile
from pymot.feller.cdf import _get_cdf

import matplotlib.pyplot as plt


# 0. Parameters
num_moments = 500
a = 2.
b = 3.

dtype = torch.float64
device= torch.device('cpu')

# 1. Compute moments

beta_dis = torch.distributions.beta.Beta(
	torch.tensor([a], dtype=dtype, device=device), 
	torch.tensor([b], dtype=dtype, device=device)
)
tens_samples = beta_dis.sample([1, 10000])
tens_samples = tens_samples.view(1, 10000)

tens_min = torch.tensor([0.], dtype=dtype, device=device)
tens_length = torch.tensor([1.], dtype=dtype, device=device)

moments = \
	estimate_moments(tens_samples, num_moments, 'feller', \
					 tens_min, tens_length, dtype, device)


f, ax = plt.subplots(2, 1, figsize=(16, 9))
ranget = np.linspace(0, 1., 2*num_moments+1, endpoint=True)
rangeq = np.linspace(0, 1., 2*num_moments+1, endpoint=True)

# 1. cdf test
cdf_hat = _get_cdf(moments, dtype, device)
ax[0].plot(ranget, np.array([cdf_hat(t) for t in ranget]), \
	linewidth=3, label="Estimated CDF")
ax[0].plot(ranget, beta.cdf(ranget, a, b), \
	label="True CDF")
ax[0].set_title('cdf estimation of a Gamma distribution with ' + str(num_moments) + ' moments', fontsize=16)
ax[0].legend()



# 2. quantile test
quantile_hat = get_func_quantile(moments, dtype, device)
ax[1].plot(rangeq, np.array([quantile_hat(q) for q in rangeq]))
#ax[1].plot(rangeq, - np.log(1 - rangeq))
ax[1].plot(rangeq, np.array([beta.ppf(q, a, b) for q in rangeq]))
ax[1].set_title('quantile function', fontsize=16)

plt.show()
