Compute moments with pymot: tutorial
====================================

This tutorial aims at demonstrating how to use the ``moments`` module of ``pymot``.
A Jupyter notebook version of this tutorial will be available soon.


Load ``pymot`` and necessary modules
------------------------------------

.. code:: python3
	
	import torch
	import numpy as np
	import matplotlib.pyplot as plt

	import pymot.moments
	import pymot.quantile

	dtype=torch.float64 # double float precision
	device = torch.device('cpu') # or cuda if available


Compute moments with ``pymot``
------------------------------

Four families of (generalized) moments are avaible in pymot (see below).
Let :math:`\mu` be a probability measure on :math:`\mathbb{R}`.


Moments can be computed with :code:`estimate_moments` from the `moments` module.
More particulary, it computes the :math:`k` first moments :math:`(m_\ell)_{\ell=0}^{k-1}`

.. code:: python3
	
	# Parameters
	type_moments = 'om' # ['om', 'laplace', 'feller', 'bins']
	num_moments = 20

	# Data
	X_samples = torch.randn([1, 500], dtype=dtype)

	# Compute the num_moments first om moments
	my_moments = pymot.moments.estimate_moments(X_samples,
						    num_moments,
						    type_moments,
						    dtype=dtype)


Family 1: Ordinary moments
~~~~~~~~~~~~~~~~~~~~~~~~~~

The ``ordinary moment`` (OM) of order :math:`\ell` is defined as

	.. math::
		:label: eq:def:om

		m_\ell \triangleq \mathbb{E}_{X\sim\mu} [X^\ell]




Family 2: Laplace moments
~~~~~~~~~~~~~~~~~~~~~~~~~~

Let :math:`\lambda` be a positive scalar and assume that :math:`\mu` is supported in :math:`\mathbb{R}_0`.
The ``laplace moment`` (laplace) of order :math:`\ell` is defined as

	.. math::
		:label: eq:def:laplace-moment

		m_\ell \triangleq \mathbb{E}_{X\sim\mu} [e^{-\lambda\ell X}]



Family 3: Feller moments
~~~~~~~~~~~~~~~~~~~~~~~~

Let :math:`\lambda` be a positive scalar and assume that :math:`\mu` is supported in :math:`\mathbb{R}_0`.
The ``feller moment`` (feller) of order :math:`\ell` is defined as

	.. math::
		:label: eq:def:feller-moment

		m_\ell \triangleq \mathbb{E}_{X\sim\mu} [e^{-\lambda X} X^\ell]


Family 4: Discretization of the quantile function
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The last family of generalized moments consists in a discretization of the quantile function / pseudo inverse of the CDF.
Let :math:`k` be a positive integer.
The :math:`\ell` bins is defined as

	.. math::
		:label: eq:def:bins-moment

		m_\ell \triangleq \frac{1}{k}\int_{\frac{\ell}{k}}^{\frac{\ell+1}{k}} Q_{\mu}(q)\,\mathrm{d}q

where :math:`Q_{\mu}` denotes the quantile function / inverse CDF associated to :math:`X\sim\mu`.


Approximate quantile function with ``pymot``
--------------------------------------------

The :code:`quantile` module provides methods to approximate a quantile function based on its moments.



.. code:: python3
	
	# Parameters
	qfunc = pymot.quantile.get_func_quantile(my_moments,
						 dtype=dtype,
						 device=device)

	# plots
	f, ax = plt.subplots(1, 1, figsize=(6, 6))

	rangeq = = np.linspace(0, 1, 201)
	ax.plt(rangeq, np.array([qfunc(q) for q in rangeq]))


``pymot`` and multidimensional random variable
----------------------------------------------

When :math:`X\sim\mu` is a random variable defined in :math:`\mathbb{R}^d` with :math:`d>1`, ``pymot`` computes the moments along each one-dimensional direction.

.. code:: python3
	
	# Parameters
	type_moments = 'om' # ['om', 'laplace', 'feller', 'bins']
	num_dim = 5
	num_moments = 20

	# Data
	X_samples = torch.randn([num_dim, 500], dtype=dtype)

	# Compute the num_moments first om moments
	my_moments = pymot.moments.estimate_moments(X_samples,
						    num_moments,
						    type_moments,
						    dtype=dtype)

	# my_moments contains (among other) a num_dim x num_moments tensor.


