.. pymot documentation master file, created by
   sphinx-quickstart on Sat Apr  4 10:11:08 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. mdinclude:: ../../readme.md
   :start-line: 0
   :end-line: 5


Documentation contents
======================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   tutorials/index


.. mdinclude:: ../../readme.md
   :start-line: 6
   :end-line: 63


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
