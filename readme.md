# Pymot: Optimal transport from moments

Pymot is an open-source python framework for solving optimal transport problem when only moments of the distribution are available. 
It supports our incoming papers.


## Requirements

Pymot Works with python 3.6+.

Dependencies:
 - [pytorch](https://pytorch.org/)
 - [NumPy](http://www.numpy.org)
 - [SciPy](https://www.scipy.org)
 - [Matplotlib](http://matplotlib.org)

To install the dependencies, simply run
``` bash
pip install torch numpy scipy matplotlib
```


## Install from sources

1. Clone the repository and enter the folder
``` bash
git clone https://gitlab.inria.fr/celvira/pymot.git
cd pymot
```


2. (Optional) Create a virtual environment and activate it
``` bash
virtualenv venv -p python3
source venv/bin/activate
```


3. And execute the `setup.py` file

``` bash
pip install .
```

or 
``` bash
pip install -e .
```
if you want it editable.


## Licence

To appear


## Cite this work

To be announced



