import argparse

import torch

import numpy as np
import matplotlib.pyplot as plt

from pymot.qregressor import QRegressorMonotonic, Trainer


# ********************************************
#
#                  Arguments
#
# ********************************************


parser = argparse.ArgumentParser(description='Sliced Wasserstein transform learning mnist')
parser.add_argument('--retrain', help='re-train existing model', action="store_true")
args = parser.parse_args()


# ********************************************
#
#                  Datas
#
# ********************************************

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
dtype = torch.float64

print('Using device ' + str(device))
  

num_samples = 1000
mid = 500

np.random.seed(240190)
vec_X = np.zeros(num_samples)
vec_X[:mid] = -1. + .25*np.random.randn(mid)
vec_X[mid:] =  1. + .25*np.random.randn(mid)


tens_samples = torch.tensor(vec_X, dtype=dtype,device=device).view(1, num_samples)



# ********************************************
#
#             Learning
#
# ********************************************

qreg = QRegressorMonotonic(
   1,
   shapes = [64, 128, 64],
   dtype=dtype,
   device=device
)

lr = 1e-3
optimizer = torch.optim.RMSprop(qreg.parameters(), lr=lr)
scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
   optimizer, 'min', patience=5)

dic_xp = {
   "num_epoch": 1000,
   "num_slices" : 1,
   "optimizer": optimizer,
   "scheduler": None, #scheduler,
   "show_plots": True,
   "file_name": None,
   "rep_plots": 10
}

trainer = Trainer(dtype, device)
trainer.train(
   tens_samples,
   qreg,
   dic_xp
)

# torch.save(qregressor.state_dict(), file_name)







