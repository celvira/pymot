import argparse

import torch
from torch.utils.data.dataset import Dataset
from torchvision import transforms

import numpy as np
import matplotlib.pyplot as plt

from pymot.qregressor import AL_QRegressor, BatchTrainer


# ********************************************
#
#                  Arguments
#
# ********************************************


parser = argparse.ArgumentParser(description='Sliced Wasserstein transform learning mnist')
parser.add_argument('--retrain', help='re-train existing model', action="store_true")
args = parser.parse_args()


# ********************************************
#
#                  Datas
#
# ********************************************

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
dtype = torch.float64

print('Using device ' + str(device))
  

class ExampleDataset(Dataset):
   """ torch-based tool to load the moment mnist
   """

   def __init__(self, tens_data, transforms=None):
      """
      Parameters
      ----------
      tens_data : torch.tensor
         size (num_dims x num_samples)
      transforms : torchvision.Transform
      """
      # stuff
      self.transforms = transforms

      # 2. Loading data
         # num-dir x 2 + num-moments

      self.tens_data = tens_data
      self.num_dims = tens_data.size(0)
      self.num_samples = tens_data.size(1)

        
   def __getitem__(self, index):
      """
      """
      data = self.tens_data[:, index]
      if self.transforms is not None:
         data = self.transforms(data)

      # If the transform variable is not empty
      # then it applies the operations in the transforms with the order that it is created.
      return (data, torch.zeros(1))


   def __len__(self):
      """
      """
      return self.num_samples



num_samples = 10000
mid = 5000

np.random.seed(240190)
vec_X = np.zeros((2, num_samples))
vec_X[:, :mid] = -1. + .25*np.random.randn(2, mid)
vec_X[:, mid:] =  1. + .25*np.random.randn(2, mid)

tens_samples = torch.tensor(vec_X, dtype=dtype,device=device).view(2, num_samples)
dataset = ExampleDataset(tens_samples)


# ********************************************
#
#             Learning
#
# ********************************************

qreg = AL_QRegressor(
   2,
   shapes = [32, 64, 32],
   dtype=dtype,
   device=device
)

lr = 1e-2
optimizer = torch.optim.RMSprop(qreg.parameters(), lr=lr)
scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
   optimizer, 'min', patience=5)

dic_xp = {
   "num_epoch": 500,
   "num_slices_per_batch" : 8,
   "optimizer": optimizer,
   "scheduler": scheduler,
   # "penalize_increments": True,
   "batch_monotonic": True,
   "show_plots": True,
   "file_name": None,
   "rep_plots": 20
}

data_loader = torch.utils.data.DataLoader(
   dataset, 
   batch_size=512, 
   shuffle=True,
)

trainer = BatchTrainer(dtype, device)
trainer.train(
   data_loader,
   qreg,
   dic_xp
)

# torch.save(qregressor.state_dict(), file_name)







