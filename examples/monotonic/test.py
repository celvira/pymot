from pymot.qregressor.qmonotonic import QMonotonic

import torch

import matplotlib.pyplot as plt

dtype = torch.float64
device= torch.device("cpu")

def init_weights(m):
    if type(m) == torch.nn.Linear:
        torch.nn.init.xavier_uniform_(m.weight)
        m.bias.data.fill_(0.01)

qreg = QMonotonic(2, shapes=[64, 32], dtype=dtype, device=device)
qreg.apply(init_weights)

n_slices = 10
tens_slices = torch.nn.functional.normalize(
   torch.randn(n_slices, 2, dtype=dtype, device=device), p=2, dim=1
)
tens_slices[:, 1] = torch.abs(tens_slices[:, 1])

tens_q = torch.linspace(0., 1., 100, dtype=dtype, device=device)

out = qreg.forward(tens_q, tens_slices)

f, ax = plt.subplots(1, 1, figsize=(6, 5))
for l in range(n_slices):
	ax.plot(tens_q.numpy(), out[l, :].detach().numpy())

plt.show()


# import chaospy

# distribution = chaospy.Uniform(0, 1)
# abscissas, weights = chaospy.generate_quadrature(
# 	100, distribution, rule="clenshaw_curtis"
# )

# print(weights.shape)