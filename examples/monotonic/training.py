from pymot.qregressor.qmonotonic import QMonotonic

import torch

import matplotlib.pyplot as plt

dtype  = torch.float64
device = torch.device("cpu")


#####################################
#
#        Datas
#
#####################################

ambiant_dim = 1
n_slices = 1
nb_samples = 200

tens_X = torch.randn(nb_samples, dtype=dtype, device=device)
# tens_X -= torch.min(tens_X)
tens_X = torch.sort(tens_X)[0].view(1, -1)

#####################################
#
#        QReg
#
#####################################

qreg = QMonotonic(ambiant_dim, shapes=[64, 32], dtype=dtype, device=device)


def init_weights(m):
    if type(m) == torch.nn.Linear:
        torch.nn.init.xavier_uniform_(m.weight)
        m.bias.data.fill_(0.5)

qreg.apply(init_weights)


#####################################
#
#        Training
#
#####################################

tens_slices = torch.nn.functional.normalize(
   torch.randn(n_slices, ambiant_dim, dtype=dtype, device=device), p=2, dim=1
)
tens_slices[:, ambiant_dim-1] = torch.abs(tens_slices[:, ambiant_dim-1])

tens_q = torch.linspace(0., 1., nb_samples, dtype=dtype, device=device)


nb_epoch = 5000
lr = 1e-3

optimizer = torch.optim.RMSprop(qreg.parameters(), lr=lr)

for epoch in range(nb_epoch):

   # tens_X = torch.randn(nb_samples, dtype=dtype, device=device)
   # tens_X = torch.sort(tens_X)[0].view(1, -1)

   # Draw neural network
   out = qreg.forward(tens_q, tens_slices)

   # Compute loss
   loss = torch.mean( (tens_X - out)**2 )

   # Update
   loss.backward() 
      
   optimizer.step()
   optimizer.zero_grad()

   if epoch % 50 == 0:
      print("Iteration {} - Loss: {}".format(epoch, loss.detach().clone().item()))
   
      plt.close()
      f, ax = plt.subplots(1, 1, figsize=(6, 5))
      ax.plot(tens_q.numpy(), tens_X.squeeze(0).detach().numpy())
      ax.plot(tens_q.numpy(), out[0, :].detach().numpy())

      ax.legend(["True qfunc", "estimated qfunc"])
      ax.set_title("Iteration {}".format(epoch))

      plt.show(block=False)
      plt.pause(.1)

