import torch

from pymot.ot import sliced_wasserstein
from pymot.moments import estimate_moments


def main():
   # 0. Example parameters
   ambiant_dim = 2
   num_samples = 500
   num_slices  = 20
   num_moments = 15
   dtype = torch.float64


   # 1. draws samples from each distributions
      # 1a. First distribution is 2D standard Gaussian
      # dim : ambiant_dim x num_samples
   tens_samples_1 = torch.randn(ambiant_dim, num_samples, dtype=dtype)

      # 1b. Second distribution is a shifted 2D standard Gaussian
      # dim : ambiant_dim x num_samples
   tens_samples_2 = 5 + torch.randn(ambiant_dim, num_samples, dtype=dtype)     


   # 2. Create slices
      # dim : num_slices x ambiant_dim
   tens_slices = torch.nn.functional.normalize(
      torch.randn(num_slices, ambiant_dim, dtype=dtype), p=2, dim=1
   )


   # 3. project samples onto slices
      # dim : num_slices x num_samples
   tens_proj_1 = tens_slices @ tens_samples_1
   tens_proj_2 = tens_slices @ tens_samples_2


   # 4. Computes moments (and other)
   moments1 = estimate_moments(tens_proj_1, num_moments, 'om')
   moments2 = estimate_moments(tens_proj_2, num_moments, 'om')


   # 5. (Finally!) Compute sliced wasserstein distance
   mot = sliced_wasserstein(
      moments1,
      moments2,
      2.
   )

   print('Estimated Sliced-Wasserstein distance : ' + str(mot.item()))
   print('\tExpected order of magnitude : 25')


if __name__ == '__main__':
   main()