import torch

from pymot.ot import sliced_wasserstein


def main():
   # 0. Example parameters
   ambiant_dim = 2
   num_samples = 5000
   num_slices  = 50
   num_moments = 15
   dtype = torch.float64
   device = torch.device('cpu')


   # 1. draws samples from each distributions
      # 1a. First distribution is 2D standard Gaussian
      # dim : ambiant_dim x num_samples
   tens_samples_1 = torch.randn(ambiant_dim, num_samples, dtype=dtype)

      # 1b. Second distribution is a shifted 2D standard Gaussian
      # dim : ambiant_dim x num_samples
   tens_samples_2 = 5 + torch.randn(ambiant_dim, num_samples, dtype=dtype)     


   # 2. Create slices
      # dim : num_slices x ambiant_dim
   tens_slices = torch.nn.functional.normalize(
      torch.randn(num_slices, ambiant_dim, dtype=dtype), p=2, dim=1
   )


   # 3. project samples onto slices
      # dim : num_slices x num_samples
   tens_proj_1 = tens_slices @ tens_samples_1
   tens_proj_2 = tens_slices @ tens_samples_2


   # 4. (Finally!) Compute sliced wasserstein distance
   mot = sliced_wasserstein(
      tens_proj_1, tens_proj_2, 2., dtype=dtype, device=device
   )

   print('Estimated Sliced-Wasserstein distance : ' + str(mot.item()))
   print('\tExpected order of magnitude : 25')


if __name__ == '__main__':
   main()