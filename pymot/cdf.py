import torch, math

from pymot.moments import Moments
from pymot.utils import _computeLogBernsteinCoeff

def get_cdf(data, dtype=torch.float64, device=torch.device('cpu')):
   """ Compute approximation of cdf based on samples or moments

    Implements [1, Eq.~(28)] but using Feller moment instead.

    [1] R. M. Mnatsakanov and A. Sborshchikovi, "Recovery of a quantile function from moments",
    Journal of Computational and Applied Mathematics  315  354 - 364  (2017) using Feller cdf estimator

   Parameters
   ----------
   data : torch.Tensor or pymot.moments.Moments
      Collection of samples / moments
   dtype : torch.dtype
      Tensor type
      Default torch.float64
   device : torch.device
      where to perform calculations

   Returns
   -------
   qfunc : lambda function 
      tensor of quantile function (one line per projection)
      size num_slices x num_moments+1
   """

   # 0. Tensor dtype
   # 0. Parameters learning / checking

   # 0. Check stuff
   if isinstance(data, Moments):

      if data.num_slices > 1:
         raise NotImplemented

      #_ 1. Compute
      if data.str_moment_name == 'om':
         from pymot.om.quantile import _get_func_quantile

         qfunc_estimator = _get_func_quantile

      elif data.str_moment_name == 'laplace':
         raise NotImplementedError

      elif data.str_moment_name == 'feller':
         from pymot.feller.quantile import _get_func_quantile
     
         qfunc_estimator = _get_func_quantile

      elif data.str_moment_name == 'bins':
         from pymot.bins.quantile import _get_func_quantile

         qfunc_estimator = _get_func_quantile

      # Returns
      if data.num_slices == 1:
         return qfunc_estimator(data, dtype, device)
      else:
         return [qfunc_estimator(data[l], dtype, device) 
            for l in range(data.num_slices)]  

    


   elif isinstance(data, torch.Tensor):

      if len( data.size() ) == 1:
         num_slices = 1
         num_samples = data.size(0)
      elif len(data.size()) == 2:
         num_slices = data.size(0)
         num_samples = data.size(1)
      else: 
         raise ValueError("data samples should be a (num_slices x num_samples) tensor")

      if num_slices == 1:
         return _get_qfunc_from_samples(data.view(1, num_samples), dtype, device)
      else:
         return [_get_qfunc_from_samples(data[l, :].view(1, num_samples), dtype, device) 
            for l in range(num_slices)]

   else:
      raise ValueError("data should be either pymot.moments.Moments or torch.Tensor")



def _get_qfunc_from_samples(one_slice, dtype, device):

   # 1. Sort samples
   num_samples = one_slice.size(1)
   sorted_samples = torch.sort(one_slice, dim=1)[0]

   # 2. Define estimator
   def qfunc(q):
      assert(q >= 0 and q <= 1)
      
      i = math.floor(q * float(num_samples))
      if i == num_samples:
         i = num_samples - 1

      return sorted_samples[0, i]

   return qfunc


def get_tens_quantile(moments, dtype=torch.float64, device=torch.device('cpu')):
   """ Compute approximation of quantile function base on Feller moments

    Implements [1, Eq.~(28)] but using Feller moment instead.

    [1] R. M. Mnatsakanov and A. Sborshchikovi, "Recovery of a quantile function from moments",
    Journal of Computational and Applied Mathematics  315  354 - 364  (2017) using Feller cdf estimator

   Parameters
   ----------
   moments : pymot.moments.Moments
      Collection of moments
   dtype : torch.dtype
      Tensor type
      Default torch.float64
   device : torch.device
      where to perform calculations

   Returns
   -------
   qfunc : lambda function 
      tensor of quantile function (one line per projection)
      size num_slices x num_moments+1
   """

   # 0. Parameters learning / checking
   assert(isinstance(moments, Moments))


   #_ 1. Compute
   if moments.str_moment_name == 'om':
      from pymot.om.quantile import _get_tens_quantile
  
      return _get_tens_quantile(moments, dtype, device)

   elif moments.str_moment_name == 'laplace':
      raise NotImplementedError

   elif moments.str_moment_name == 'feller':
      from pymot.feller.quantile import _get_tens_quantile
  
      return _get_tens_quantile(moments, dtype, device)

   elif moments.str_moment_name == 'bin':
      from pymot.feller.quantile import _get_tens_quantile
  
      return _get_tens_quantile(moments, dtype, device)
