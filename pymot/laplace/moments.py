import torch


def estimate_moments(tens_project_samples, num_moments, b,
   dtype=torch.float64, device=torch.device('cpu')):
   """ Compute the :math:`k` first Laplace moments associated to a set of samples
    
   .. math::
        m_\\ell = \\mathbb{E}_X\\left[ e^{-\\ell\\log(b)X} \\right]

   where :math:`X` is the uniform distribution on :math:`\\mathbb{R}_+`.

   Parameters
   ----------
   tens_project_samples : torch.Tensor
      projected sampels
      num-slices x num_samples
   num_moments : float
      number of desired moments
   b : positive float
      scale parameter of the laplace transform
   dtype : torch.dtype
      Tensor type (default: torch.float64)
   device : torch.device
      Desired device (default: cpu)

   Returns
   -------
   tens_moments : torch.Tensor
      size num_slices x num_moments
   tens_min : torch.Tensor
      size num_slices
   """

   assert(b > 1)

   num_slices  = tens_project_samples.size(0)
   num_samples = tens_project_samples.size(1)


   # 1. Min and length
   tens_min = torch.min(tens_project_samples, dim=1, keepdim=False)[0]


   # 2. Moments
   log_b = torch.log(torch.tensor(b, dtype=dtype, device=device))
   tens_l_log_b = log_b * torch.arange(num_moments, dtype=torch.float64, device=device)
   tens_moments = torch.stack([torch.mean(
         torch.exp( u * (tens_project_samples - tens_min.view(num_slices, 1).repeat(1, num_samples) ))
      , dim=1) for u in tens_l_log_b
   ]).T


   # 3. Output
   return (tens_moments, tens_min)


def compute_exponential_moments(num_moments, lbd, b,
   dtype=torch.float64, device=torch.device('cpu')):
   """ Compute the :math:`k` first moments of
   the uniform distribution on :math:`[0,T]` given by
    
   .. math::
      m_\\ell = \\mathbb{E}_U\\left[ X^\\ell \\right] \\quad j=0\\dots k-1.

   where :math:`U` is the uniform distribution on :math:`[0,T]`.

   Parameters
   ----------
   num_moments : positive integer
      number of desired moments
   lbd : positive float
      scale paramater of the exponential distribution
   b : positive float
      scale parameter of the laplace transform

   Returns
   -------
   tens_moments : torch.tensor
      vector of laplace moments
      size num_moments
   """
    
   assert(num_moments > 0)
   assert(lbd > 0)
   assert(b > 0)

   tens_lbd  = torch.tensor(lbd, dtype=dtype, device=device)
   log_b = torch.log(torch.tensor(b, dtype=dtype, device=device))
   tens_l_log_b = log_b * torch.arange(num_moments, dtype=dtype, device=device)

   tens_moments = tens_lbd / (tens_lbd + tens_l_log_b)

   return tens_moments


if __name__ == '__main__':
   # Parameters
   num_slices = 10
   num_samples = 20
   num_moments = 5

   # Data
   tens_A = torch.randn([num_slices, num_samples], dtype=torch.float64)

   # Call function
   print(
      estimate_moments(tens_A, num_moments, 1.5)
   )

