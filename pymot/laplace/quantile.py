def inv_laplace_quantile(bfl, lbd):
    """ Return estimator defined in [Feller, Eq.~6.4]
    """

    assert(bfl.ndim == 1)
    assert(lbd > 0)

    k = bfl.size - 1.
    xmax = float(k) / lbd

    # 1. Precompute all coefficients
    rangeint = np.arange(k+1)
    coeff_cdf = np.exp(rangeint * np.log(lbd) - gammaln(rangeint+1)) * bfl

    vec_cdf = np.cumsum(coeff_cdf)

    # 2. Define quantile estimator
    def estimator(x):

        assert(x >= 0)
        assert(x < 1.)

        return np.argmax(vec_cdf >= x) / k

    return estimator