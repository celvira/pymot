import torch, math

from pymot.utils import _computeLogCDFmat

def get_cdf(tens_moments, tens_offset, b, 
   dtype=torch.float64, device=torch.device('cpu')):
   """ Return an estimator of the cdf of a probability distribution based on Laplace moments
    
   Implements [1, eq.~(27)].

   [1] R. M. Mnatsakanov and A. Sborshchikovi, "Recovery of a quantile function from moments",
   Journal of Computational and Applied Mathematics  315  354 - 364  (2017)

   Parameters
   ----------
   tens_moments : torch.Tensor
      vector of Laplace moment
      with the convention that bfl[0] = 1 (laplace function evaluated at 0)
      size num_slices x num_moments
   tens_offset : torch.tensor
      tensor of offsets for each directions
   b: float
      parameter of the laplace moment
   dtype : torch.dtype
      Tensor type
      Default torch.float64
   device : torch.device
      where to perform calculations

   Returns
   -------
   cdf: lambda x |-> cdf(x)
      estimator of the cdf
   """

   # 0. Parameters learning / checking
   assert(b > 1)

   if len(tens_moments.size()) == 1:
      num_slices  = 1
      num_moments = tens_moments.size(0) - 1

   elif len(tens_moments.size()) == 2:
      num_slices  = tens_moments.size(0)
      num_moments = tens_moments.size(1) - 1

   else:
      ValueError("tens_moments must a (num_slices, num_moments) Tensor")

   assert(tens_offset.size() == torch.Size([num_slices]))

   if num_slices > 1:
      raise NotImplemented

   # 1. design matrices for fast computation
   tens_buf_log_cdf, tens_buf_sign_cdf = _computeLogCDFmat(num_moments, dtype, device)

   #tens_cdf = tens_bufcdf @ tens_moments.view(num_slices, num_moments+1)
   tens_cdf = torch.stack([
      (torch.exp(
         tens_buf_log_cdf + tens_moments.view(num_slices, num_moments+1)[l, :].view(1, num_moments+1).repeat(num_moments+1, 1).log()
      ) * tens_buf_sign_cdf)
      @ torch.ones(num_moments + 1, dtype=dtype, device=device)
      for l in range(num_slices)], dim=0)
         # torch.exp(tens_moments.view(num_slices, num_moments+1) @ tens_buf_log_cdf.T)
         # * tens_buf_sign_cdf,

   tens_cdf = torch.cumsum(tens_cdf, dim=1)

   print(tens_cdf)
   # exit()

   # 2. Define cdf estimator
   def estimator(x):        
      assert(x >= 0)

      x_off = x - tens_offset[0].item()

      if x_off == 0:
         return 0
      else:
         floor = int(math.floor(num_moments * b**(-x_off)))
         return 1 - tens_cdf[0, floor]

   # 4. Return
   return estimator


def get_tens_cdf(tens_moments, b, 
   dtype=torch.float64, device=torch.device('cpu')):
   """ Return an estimator of the cdf of a probability distribution based on Laplace moments
    
   Implements [1, eq.~(27)].

   [1] R. M. Mnatsakanov and A. Sborshchikovi, "Recovery of a quantile function from moments",
   Journal of Computational and Applied Mathematics  315  354 - 364  (2017)

   Parameters
   ----------
   tens_moments : torch.Tensor
      vector of Laplace moment
      with the convention that bfl[0] = 1 (laplace function evaluated at 0)
      size num_slices x num_moments
   b: float
      parameter of the laplace moment
   dtype : torch.dtype
      Tensor type
      Default torch.float64
   device : torch.device
      where to perform calculations

   Returns
   -------
   cdf: lambda x |-> cdf(x)
      estimator of the cdf
   """

   # 0. Parameters learning / checking
   assert(b > 1)

   if len(tens_moments.size()) == 1:
      num_slices  = 1
      num_moments = tens_moments.size(0) - 1

   elif len(tens_moments.size()) == 2:
      num_slices  = tens_moments.size(0)
      num_moments = tens_moments.size(1) - 1

   else:
      error()


   # 1. design matrices for fast computation
   tens_bufcdf = _computeCDFmat(num_moments, dtype, device)
   tens_cdf = tens_bufcdf @ tens_moments.view(num_slices, num_moments)























def quantile(bfm, b):
    """ Return an estimator of the cdf function based from moments
    
    Defined for compactly supported distributions

    Implements [1, Eq.~(28)].

    [1] R. M. Mnatsakanov and A. Sborshchikovi, "Recovery of a quantile function from moments",
    Journal of Computational and Applied Mathematics  315  354 - 364  (2017)

    Parameters
    ----------
    bfl: np.ndarray
        vector of Laplace moment
        with the convention that bfl[0] = 1 (laplace function evaluated at 0)
    b: float
        a
    
    Returns
    -------
    cdf: lambda x |-> cdf(x)
        a lambda function
    """

    assert(bfm.ndim == 1)
    assert(b > 1)

    # 1. Number of moments
    k = bfm.size - 1 # -1 because moment 0 does not ocunt

    let_gamln1 = gammaln(k + 1.)

    cdf_est = cdf(bfm, b)

    # 2. design matrices for computation
    bfQ = np.zeros((k+1, k))
    for i in range(k+1):
        for j in range(k):
            if i == 0:
                x = 0
            else:
                x = ( np.log(k) - np.log(i) ) / np.log(b)

            i_logF = i * np.log(cdf_est(x))
            i_logF = - np.inf if np.isnan(i_logF) else i_logF
            i_log1mF = (k-i) * np.log(1 - cdf_est(x))
            i_log1mF = - np.inf if np.isnan(i_log1mF) else i_log1mF

            bfQ[i, j] = np.exp( \
                let_gamln1 - gammaln(i + 1.) - gammaln(k - i + 1.)\
                + i_logF \
                + i_log1mF
            )

    size_inter = np.zeros(k+1)
    size_inter[0] = np.log(k) / np.log(b)
    for i in range(1, k):
        size_inter[i] = (np.log(i+1) - np.log(i)) / np.log(b)

    vec_qf = np.cumsum((bfQ @ np.ones(k)) * size_inter)

    # 3. Define cdf estimator
    def estimator(x):        
        assert(x >= 0)
        floor = int(np.floor(k * b**(-x)))

        return vec_qf[floor]

    # 4. Return
    return estimator



def inv_laplace_cdf(bfl, lbd):
    """ Return estimator defined in [Feller, Eq.~6.4]
    """

    assert(bfl.ndim == 1)
    assert(lbd > 0)

    k = bfl.size - 1.
    xmax = float(k) / lbd

    # 1. Precompute all coefficients
    rangeint = np.arange(k+1)
    coeff_cdf = np.exp(rangeint * np.log(lbd) - gammaln(rangeint+1)) * bfl

    vec_cdf = np.cumsum(coeff_cdf)

    # 2. Define cdf estimator
    def estimator(x):

        assert(x >= 0)
        assert(x < xmax)

        floor = int(np.floor(lbd * x))

        return vec_cdf[floor]


    return estimator


def inv_laplace_quantile(bfl, lbd):
    """ Return estimator defined in [Feller, Eq.~6.4]
    """

    assert(bfl.ndim == 1)
    assert(lbd > 0)

    k = bfl.size - 1.
    xmax = float(k) / lbd

    # 1. Precompute all coefficients
    rangeint = np.arange(k+1)
    coeff_cdf = np.exp(rangeint * np.log(lbd) - gammaln(rangeint+1)) * bfl

    vec_cdf = np.cumsum(coeff_cdf)

    # 2. Define quantile estimator
    def estimator(x):

        assert(x >= 0)
        assert(x < 1.)

        return np.argmax(vec_cdf >= x) / k

    return estimator
