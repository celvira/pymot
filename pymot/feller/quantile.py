import torch, math

from pymot.feller.moments import FellerMoments
from pymot.feller.cdf import _get_tens_cdf
from pymot.utils import _computeLogBernsteinCoeff


def _get_func_quantile(moments, dtype=torch.float64, device=torch.device('cpu')):
   """ Compute approximation of quantile function base on Feller moments

    Implements [1, Eq.~(28)] but using Feller moment instead.

    [1] R. M. Mnatsakanov and A. Sborshchikovi, "Recovery of a quantile function from moments",
    Journal of Computational and Applied Mathematics  315  354 - 364  (2017) using Feller cdf estimator

   Parameters
   ----------
   moments : pymot.feller.moments.FellerMoments
      Collection of moments
   dtype : torch.dtype
      Tensor type
      Default torch.float64
   device : torch.device
      where to perform calculations

   Returns
   -------
   qfunc : lambda function 
      tensor of quantile function (one line per projection)
      size num_slices x num_moments+1
   """

   # 0. Tensor dtype
   # 0. Parameters learning / checking
   assert(isinstance(moments, FellerMoments))

   if moments.num_slices > 1:
      raise NotImplemented

   # 1. Tensor quantile
   tens_quantile = \
      _get_tens_quantile(moments, dtype, device)


   # 3. Quantile function
   def estimator(q):
      if q == 0:
         return moments.tens_min.item()

      elif q >= 1:
         return moments.tens_min.item() + moments.tens_length.item()

      floor = int(math.floor(moments.num_moments * q))

      return tens_quantile[0, floor]

   return estimator


def _get_tens_quantile(moments, dtype=torch.float64, device=torch.device('cpu')):
   """ Compute approximation of quantile function base on Feller moments

    Implements [1, Eq.~(28)] but using Feller moment instead.

    [1] R. M. Mnatsakanov and A. Sborshchikovi, "Recovery of a quantile function from moments",
    Journal of Computational and Applied Mathematics  315  354 - 364  (2017) using Feller cdf estimator

   Parameters
   ----------
   moments : pymot.feller.moments.FellerMoments
      Collection of moments
   dtype : torch.dtype
      Tensor type
      Default torch.float64
   device : torch.device
      where to perform calculations

   Returns
   -------
   qfunc : lambda function 
      tensor of quantile function (one line per projection)
      size num_slices x num_moments+1
   """

   # 0. Checking and parameters
   assert(isinstance(moments, FellerMoments))

   num_moments = moments.num_moments
   num_slices  = moments.num_slices

   # 1. Tensor cdf
   tens_cdf = \
      _get_tens_cdf(moments, dtype, device)

   tens_log_bernstein = _computeLogBernsteinCoeff(num_moments, dtype, device)
   tens_range = torch.arange(0, num_moments+1, dtype=dtype, device=device)

   tens_quantile = torch.stack(
      [torch.sum(torch.exp(
         tens_log_bernstein.view(1, num_moments + 1).repeat(num_moments + 1, 1)
         + tens_cdf[l, :].log().view(num_moments+1, 1) @ tens_range.view(1, num_moments + 1) 
         + (1. - tens_cdf[l, :]).log().view(num_moments+1, 1) @ (num_moments - tens_range).view(1, num_moments + 1)
      ), dim=0) for l in range(num_slices)], dim=0
   )

   tens_quantile = torch.diag(1. / moments.tens_lbd.view(num_slices)) @ torch.cumsum(tens_quantile, dim=1)
   tens_quantile += moments.tens_min.view(num_slices, 1).repeat(1, num_moments+1)

   return tens_quantile
