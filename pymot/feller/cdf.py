import torch, math

from pymot.utils import _computeLogFellerVec
from pymot.feller.moments import FellerMoments

def _get_cdf(moments, dtype=torch.float64, device=torch.device('cpu')):
   """ Return an estimator of the cdf of a probability distribution based on Laplace moments
    
   Implements [Feller II, eq.~(6.4)].

   [1] R. M. Mnatsakanov and A. Sborshchikovi, "Recovery of a quantile function from moments",
   Journal of Computational and Applied Mathematics  315  354 - 364  (2017)

   Parameters
   ----------
   moments : pymot.feller.moments.FellerMoments
      Collection of moments
   dtype : torch.dtype
      Tensor type
      Default torch.float64
   device : torch.device
      where to perform calculations

   Returns
   -------
   cdf: lambda x |-> cdf(x)
      estimator of the cdf
   """

   # 0. Parameters learning / checking
   assert(isinstance(moments, FellerMoments))

   if moments.num_slices > 1:
      raise NotImplemented

   # 1. design matrices for fast computation
   tens_cdf = \
      _get_tens_cdf(moments, dtype, device)


   # 2. Define cdf estimator
   def estimator(x):        
      x_off = x - moments.tens_min[0].item()
      assert(x_off >= 0)

      if x_off == 0:
         return 0
      else:
         floor = int(math.floor(moments.tens_lbd[0] * x_off))
         return tens_cdf[0, floor]

   # 4. Return
   return estimator


def _get_tens_cdf(moments, dtype=torch.float64, device=torch.device('cpu')):
   """ Return an estimator of the cdf of a probability distribution based on Laplace moments
    
   Implements [Feller II, eq.~(6.4)].

   [1] R. M. Mnatsakanov and A. Sborshchikovi, "Recovery of a quantile function from moments",
   Journal of Computational and Applied Mathematics  315  354 - 364  (2017)

   Parameters
   ----------
   moments : pymot.feller.moments.FellerMoments
      Collection of moments
   dtype : torch.dtype
      Tensor type
      Default torch.float64
   device : torch.device
      where to perform calculations

   Returns
   -------
   cdf: lambda x |-> cdf(x)
      estimator of the cdf
   """

   # 0. Parameters learning / checking
   assert(isinstance(moments, FellerMoments))
   num_moments = moments.num_moments
   num_slices  = moments.num_slices


   # 1. design matrices for fast computation
   tens_buf_log_cdf = _computeLogFellerVec(num_moments, dtype, device)
   tens_range = torch.arange(0, num_moments+1, dtype=dtype, device=device)


   # 3. Compute cdf
   tens_cdf = torch.cumsum(torch.exp(
      moments.tens_lbd.log().view(num_slices, 1) @ tens_range.view(1, num_moments+1) \
      - tens_buf_log_cdf.view(1, num_moments+1).repeat(num_slices, 1)
      + moments.tens_moments.log()
   ), dim=1)


   # 3. Return
   return tens_cdf

