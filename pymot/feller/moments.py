import torch

from pymot.moments import Moments


class FellerMoments(Moments):
   """ Class containing the FellerMoments
   ..math::
      m_\\ell = \\mathbb{E}_X\\left[ e^{-\\lambda X} X^\\ell \\right]
   for \\ell ranging from 0 to num_moments - 1

   Parameters
   ----------
   tens_moments : torch.Tensor
      matrice of moments
      size num_slices x num_moments
   tens_min : torch.Tensor
      size num_slices
   tens_length : torch.Tensor
      size num_slices
   tens_lambda = torch.Tensor
      parameters of the Feller moments (per slice)
      size num_slices

   Attributes
   ----------
   tens_moments : torch.Tensor
      size num_slices x num_moments
   tens_min : torch.Tensor
      size num_slices
   tens_length : torch.Tensor
      size num_slices
   num_slices : int
      number of slices
   num_moments : int
      total number of moment
   str_moment_name : str
      name of the family of moments
   """
   def __init__(self, tens_moments, tens_min, tens_length, tens_lbd,
                dtype=torch.float64, device=torch.device('cpu')):

      super(FellerMoments, self).__init__(
         tens_moments,
         tens_min,
         tens_length,
         dtype,
         device)

      self.tens_lbd = tens_lbd.view(self.num_slices)

      self.str_moment_name = 'feller'


   def __getitem__(self, key):
      return FellerMoments(
         self.tens_moments[key, :],
         self.tens_min[key],
         self.tens_length[key],
         self.tens_lbd[key],
         self.dtype,
         self.device
      )


def _estimate_feller_moments(tens_project_samples, num_moments,
   tens_min, tens_length,
   dtype=torch.float64, device=torch.device('cpu')):
   """ Compute the :math:`k` first Laplace moments associated to a set of samples
    
   .. math::
        m_\\ell = \\mathbb{E}_X\\left[ e^{-\\ell\\log(b)X} \\right]

   where :math:`X` is the uniform distribution on :math:`\\mathbb{R}_+`.

   Parameters
   ----------
   tens_project_samples : torch.Tensor
      projected sampels
      num-slices x num_samples
   num_moments : float
      number of desired moments
   tens_min : torch.Tensor
      size num_slices
   tens_length : torch.Tensor
      size num_slices
   dtype : torch.dtype
      Tensor type (default: torch.float64)
   device : torch.device
      Desired device (default: cpu)

   Returns
   -------
   tens_moments : torch.Tensor
      size num_slices x num_moments
   tens_min : torch.Tensor
      size num_slices
   tens_length : torch.Tensor
      size num_slices
   """

   num_slices  = tens_project_samples.size(0)
   num_samples = tens_project_samples.size(1)


   # 1. Moments
   tens_lbd = torch.tensor(num_moments-1., dtype=dtype, device=device) / tens_length

   tens_moments = torch.stack([torch.tensor(
         [torch.mean(torch.exp(
            - tens_lbd[l] * (tens_project_samples[l, :] - tens_min[l])
            + float(k) * torch.log(tens_project_samples[l, :] - tens_min[l] + 1e-32)
         )) for k in range(num_moments)], dtype=dtype, device=device)
      for l in range(num_slices)], dim = 0)


   # 2. Output
   return FellerMoments(tens_moments, tens_min, tens_length,
                         tens_lbd, dtype, device)


if __name__ == '__main__':
   # Parameters
   num_slices = 10
   num_samples = 20
   num_moments = 5

   # Data
   tens_A = torch.randn([num_slices, num_samples], dtype=torch.float64)

   # Call function
   print(
      estimate_moments(tens_A, num_moments, 1.5)
   )

