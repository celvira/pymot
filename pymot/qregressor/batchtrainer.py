import torch

from pymot.qregressor.atrainer import ATrainer
from pymot.qregressor.aqregressor import AQRegressor

import matplotlib.pyplot as plt

class BatchTrainer(ATrainer):
   """
      Train a neural network witjour resoprin
   """

   def __init__(self, dtype=torch.float64, device=torch.device('cpu')):
      """
      """

      super(BatchTrainer, self).__init__(dtype, device)


   def train(self, data_object, qregressor, dic_params):
      """
      loss: torch.nn.MSELoss

      Parameters
      ----------
         data_object : torch.utils.data.DataLoader
            dataloader object
         dic_params : python dictionary
            keys
             -> num_epoch : int
               Number of iterations
             -> num_slices : int
               number of slices
             -> optimizer : torch.optimizer
             -> scheduler : torch scheduler
               (Optional, default is None)
             -> show_plot : bool
               (Optional, default is True)
             -> keep_slices : bool
               Keep the same slice all along the optimization process
               (Optional, default if False)
             -> file_name : str
               filename to save NN
               (Optional, default is None)
             -> rep_plots : int
               every how many iterations one has to plot stuffs
               (Optional, deault is num_epoch/100)

      """

      ##################################
      #
      #        Parameters stuff
      #
      ##################################

      assert(isinstance(data_object, torch.utils.data.DataLoader))
      assert(isinstance(qregressor, AQRegressor))
      assert(isinstance(dic_params, dict))


      ##################################
      #
      #        Optimization stuff
      #
      ##################################

      # Mandatory arguments
      num_epoch = dic_params["num_epoch"]
      num_slices_per_batch = dic_params["num_slices_per_batch"]
      optimizer = dic_params["optimizer"]

      # Optional arguments
      penalize_increments = False if "penalize_increments" not in dic_params \
         else dic_params["penalize_increments"]

      batch_monotonic = False if "batch_monotonic" not in dic_params \
         else dic_params["batch_monotonic"]

      scheduler = None if "scheduler" not in dic_params \
         else dic_params["scheduler"]

      file_name = None if "file_name" not in dic_params \
         else dic_params["file_name"]

      keep_slices = False if "keep_slices" not in dic_params \
         else dic_params["keep_slices"]

      show_plot   = True if "show_plots" not in dic_params \
         else dic_params["show_plots"]

      rep_plots   = int(num_epoch/100) if "rep_plots" not in dic_params \
         else dic_params["rep_plots"]


      ##################################
      #
      #           Trainings
      #
      ##################################

      loss_func = torch.nn.MSELoss(reduction='mean')

      # Learning phase
      losses=[]
      qregressor.train()
      for epoch in range(num_epoch + 1):

         num_batch = len(data_object)
         epoch_losses = torch.zeros(num_batch)
         for batch_idx, (batch_samples, _) in enumerate(data_object, start=0):    

            num_samples_per_batch = batch_samples.size(0)
            list_dims = batch_samples.size()[1:]
            num_dims  = torch.prod(torch.tensor(list_dims), 0).item()

            if num_dims == 1:
               keep_slices = True # since there is no slices anymore

            true_q = torch.arange(1, num_samples_per_batch+1, dtype=self._dtype, device=self._device) / num_samples_per_batch

            # 1. Slice related work
            if (epoch == 0 and batch_idx == 0) or not keep_slices:
               # 1a. Generate slices
               tens_slices = torch.nn.functional.normalize(
                  torch.randn(
                     num_slices_per_batch, 
                     num_dims, 
                     dtype=self._dtype, 
                     device=self._device
                  ),
                  p=2, dim=1
               )
               tens_slices[:, num_dims-1] = torch.abs(tens_slices[:, num_dims-1])
               tens_slices.requires_grad = True
               # tens_slices = tens_slices.view(torch.Size([num_slices_per_batch]) + list_dims)

               # 1b. Project data onto slices
               tens_proj = tens_slices @ batch_samples.view(num_dims, num_samples_per_batch)

               # 1c. Compute quantile function for each slice
               sorted_samples = torch.sort(tens_proj, dim=1)[0]

            # zero the gradients
            optimizer.zero_grad()

            # 2. compute regressed values (slices_batchsize, samples_batchsize)
            x_pred = qregressor.forward(true_q, tens_slices)


            # 3. loss evaluation, 
            loss = loss_func(sorted_samples, x_pred)

            # decreasing variations penalized
            if penalize_increments:
               negative_increments = torch.exp(torch.relu(
                  x_pred[:, :-1] - x_pred[:, 1:]
                  )
               )
               loss += torch.mean(negative_increments)

            elif batch_monotonic:
               diff = x_pred[:, 1:] - x_pred[:, :-1]
               rectified = torch.relu(diff) + 1e-4
               x_pred = torch.cumsum(
                   torch.cat((x_pred[:, 0, None], rectified), dim=1),
                   dim=1
               )


            epoch_losses[batch_idx] = loss.clone().detach().cpu()
            loss.backward() 
             
            optimizer.step()

            if scheduler is not None:
               scheduler.step(loss)

            # 4. Eventualy plot stuff
            if show_plot and batch_idx % rep_plots == 0:
               print("Iteration{}. Loss={:4e}".format(epoch, loss.clone().detach().cpu().numpy()))

               max_slices = 4

               plt.close()
               fig, ax = plt.subplots(1, min(num_slices_per_batch, max_slices), figsize=(min(num_slices_per_batch, max_slices)*4, 4))
               for i in range(min(num_slices_per_batch, max_slices)):

                  if min(num_slices_per_batch, max_slices) == 1:
                     myax = ax
                  else:
                     myax = ax[i]

                  # x_pred = self.forward(true_q.unsqueeze(1), tens_slices_batch[i,:].view(1, num_dims).repeat(num_samples, 1))
                  myax.plot(true_q.clone().cpu().numpy(), sorted_samples[i,:].clone().detach().cpu().numpy(),'.',c='r',label='True')
                  myax.plot(true_q.clone().cpu().numpy(), x_pred[i, :].clone().detach().cpu().numpy(),'.',c='b',label='Predicted')
                  myax.legend(loc='best')

               plt.tight_layout()
               plt.show(block=False)
               plt.pause(.1)

            # 5. Eventually save
            self.save_model(qregressor, file_name)

         losses.append(epoch_losses.mean().item())

      qregressor.eval()
