from pymot.qregressor.aqregressor import AQRegressor

import torch
import chaospy

class QMonotonic(AQRegressor):
   """My implementation of of the Monotonic Qregressor"""

   def __init__(self, input_dim, shapes=[256, 128, 64], nb_steps_integration=200, dtype=torch.float64, device=torch.device('cpu')):
      """
         Create a QRegressor network

         Parameters:
         -----------
         input_dim: int
            the slices dimension
         shapes: list of ints
            every entry gives a hidden size for the network.
      """
      super(QMonotonic, self).__init__(dtype, device)

      distribution = chaospy.Uniform(0, 1)
      abscissas, weights = chaospy.generate_quadrature(
         nb_steps_integration-1, distribution, rule="clenshaw_curtis"
      )

      self.abscissas = torch.from_numpy(abscissas).type(dtype).to(device)
      self.weights = torch.from_numpy(weights).type(dtype).to(device)
      self.nb_steps_integration = nb_steps_integration

      # adding input and output dimensions
      shapes = [input_dim + 1,] + shapes + [1,]

      # creating the linear layers
      # self.layers = torch.nn.ModuleList([
      #    torch.nn.Linear(shapes[k], shapes[k+1], bias = k<len(shapes)-1).type(dtype).to(device)
      #    for k in range(len(shapes)-1)
      # ])
      self.net = []
      for h0, h1 in zip(shapes, shapes[1:]):
         self.net.extend([
            torch.nn.Linear(h0, h1).type(dtype).to(device),
            torch.nn.ReLU().type(dtype).to(device),
         ])
      self.net.pop()  # pop the last ReLU for the output layer
      # It will output the scaling and offset factors.
      self.net = torch.nn.Sequential(*self.net)
      
      # Net offset
      shapes_offset = [input_dim, 32, 16, 1,]
      self.net2 = []
      for h0, h1 in zip(shapes_offset, shapes_offset[1:]):
         self.net2.extend([
            torch.nn.Linear(h0, h1).type(dtype).to(device),
            torch.nn.ReLU().type(dtype).to(device),
         ])
      self.net2.pop()  # pop the last ReLU for the output layer
      # It will output the scaling and offset factors.
      self.net2 = torch.nn.Sequential(*self.net2)

   def forward(self, q, slices):
      """
      Parameters:
      -----------
      quantiles: (n_quantiles,)
         the quantiles to estimate
      slices: (n_slices, input_dim):
         the slices parmeters along which we want
         the quantiles. 

      Returns:
      --------
      (n_slices, n_quantiles) tensor
         desired quantiles
      """

      # get the dimensions
      q = q.flatten()
      n_quantiles = q.numel()
      n_slices, ambiant_dim = slices.shape

      # making q (n_slices, n_quantiles, nb_evaluation)
      # q = q.view(1, n_quantiles, 1).repeat((n_slices, 1, self.nb_steps_integration))
      xeval = self.abscissas.view(1, self.nb_steps_integration).expand((n_quantiles, -1)) \
         * q.view(n_quantiles, 1).expand((-1, self.nb_steps_integration))


      def _eval(x, with_relu=True):
         # apply all layers and a relu activation

         y = self.net(x)         
         if with_relu:
            return torch.relu(y).flatten()
         else:
            return y.flatten()


         # Eval NN + clenshaw curtis integration
      feval = torch.cat([
         self.weights @ _eval(torch.cat((
            xeval.view(-1, 1), 
            slices[l, :].view(1, 1, ambiant_dim).expand((n_quantiles, self.nb_steps_integration, -1)).view(-1, ambiant_dim)
            ), 1
         )).view(self.nb_steps_integration, n_quantiles)
         for l in range(n_slices)], 0
      )
      
         # Offset
      # f0 = _eval(torch.cat(
      #    (torch.zeros(n_slices, 1, dtype=self._dtype, device=self._device), slices), 1
      # ), with_relu=False)
      f0 = self.net2(slices)

      # out is (num_slices x n_quantiles)
      return q.view(1, n_quantiles).expand(n_slices, -1) * feval.view(n_slices, n_quantiles) \
         + f0.view(n_slices, 1).expand(-1, n_quantiles)


   def train(self):
      for param in self.net.parameters():
         self.net.requires_grad = True

      for param in self.net2.parameters():
         self.net2.requires_grad = True

   def eval(self):
      for param in self.net.parameters():
         self.net.requires_grad = False

      for param in self.net2.parameters():
         self.net2.requires_grad = False
