from pymot.qregressor.aqregressor import AQRegressor

import torch


class Normalized_QRegressor(AQRegressor):
   """
      QRegressor network, to predict quantiles from slices
   """

   def __init__(self, input_dim, shapes, dtype, device):
      """
         Create a QRegressor network

         Parameters:
         -----------
         input_dim: int
           the slices dimension
         shapes: list of ints
           every entry gives a hidden size for the network.
           ex: [256, 128, 64]
      """
   
      super(Normalized_QRegressor, self).__init__(dtype, device)

      # adding input and output dimensions
      shapes = [input_dim + 1,] + shapes + [1,]

      # creating the linear layers
      self.layers = torch.nn.ModuleList([
         torch.nn.Linear(shapes[k], shapes[k+1], bias = k<len(shapes)-1).type(dtype)
         for k in range(len(shapes)-1)
      ])


      # creating the linear layers
      shapes2 = [8, 4]
      shapes2 = [input_dim,] + shapes2 + [1,]
      self.layers2 = torch.nn.ModuleList([
         torch.nn.Linear(shapes2[k], shapes2[k+1], bias = k<len(shapes2)-1).type(dtype)
         for k in range(len(shapes2)-1)
      ])


   def forward(self, q, slices):
      """
      Parameters:
      -----------
      quantiles: (n_quantiles,)
         the quantiles to estimate
      slices: (n_slices, input_dim):
         the slices parmeters along which we want
         the quantiles. 

      Returns:
      --------
      (n_slices, n_quantiles) tensor
         desired quantiles
      """

      # get the dimensions
      q = q.flatten()
      n_quantiles = q.numel()
      n_slices, ambiant_dim = slices.shape

      # making q (n_slices, n_quantiles)
      q = q[None].repeat((n_slices, 1))

      # making slices (n_slices, n_quantiles, dimspace)
      slices = slices[:, None, :].repeat((1, n_quantiles, 1))

      # reshape all to have first dimension n_slices * n_quantiles
      q = q.view(-1, 1)
      slices = slices.view(-1, ambiant_dim)

      # input of the actual network:
      x = torch.cat((q, slices), 1)
      # apply all layers and a relu activation
      for layer in self.layers[:-1]:
         x = layer(x)
         x = torch.relu(x)

      # output layer (no activation)
      x = self.layers[-1](x)
      x = torch.tanh(x)
      
      # apply all layers and a relu activation
      y = slices
      for layer in self.layers2[:-1]:
         y = layer(y)
         y = torch.relu(y)

      # output layer (no activation)
      y = self.layers2[-1](y)

      return x.view(n_slices, n_quantiles) * y.view(n_slices, n_quantiles)

