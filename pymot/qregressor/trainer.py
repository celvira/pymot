from abc import ABC, abstractmethod

import torch

from pymot.qregressor.atrainer import ATrainer
from pymot.qregressor.aqregressor import AQRegressor

import matplotlib.pyplot as plt

class Trainer(ATrainer):
   """
      Train a neural network witjour resoprin
   """

   def __init__(self, dtype=torch.float64, device=torch.device('cpu')):
      """
      """

      super(Trainer, self).__init__(dtype, device)


   def train(self, data_object, qregressor, dic_params):
      """
      loss: torch.nn.MSELoss

      Parameters
      ----------
         data_object : 
            contains tensor of datas
            last dimension is number of samples
         dic_params : python dictionary
            keys
             -> num_epoch : int
               Number of iterations
             -> num_slices : int
               number of slices
             -> optimizer : torch.optimizer
             -> scheduler : torch scheduler
               (Optional, default is None)
             -> show_plot : bool
               (Optional, default is True)
             -> keep_slices : bool
               Keep the same slice all along the optimization process
               (Optional, default if False)
             -> file_name : str
               filename to save NN
               (Optional, default is None)
             -> rep_plots : int
               every how many iterations one has to plot stuffs
               (Optional, deault is num_epoch/100)

      """

      ##################################
      #
      #        Parameters stuff
      #
      ##################################

      assert(isinstance(data_object, torch.Tensor))
      assert(isinstance(qregressor, AQRegressor))
      assert(isinstance(dic_params, dict))

      if len(data_object.size()) == 1:
         num_dims    = 1
         num_samples = data_object.size(0)

      elif len(data_object.size()) == 2:
         num_dims    = data_object.size(0)
         num_samples = data_object.size(1) 

      else:
         raise NotImplemented("Not implemented when datas is not a (num_dim x num_samples) torch.Tensor")

      ##################################
      #
      #        Optimization stuff
      #
      ##################################

      # Mandatory arguments
      num_epoch = dic_params["num_epoch"]
      num_slices = dic_params["num_slices"]
      optimizer = dic_params["optimizer"]

      # Optional arguments
      scheduler = None if "scheduler" not in dic_params \
         else dic_params["scheduler"]

      file_name = None if "file_name" not in dic_params \
         else dic_params["file_name"]

      keep_slices = False if "keep_slices" not in dic_params \
         else dic_params["keep_slices"]

      show_plot   = True if "show_plots" not in dic_params \
         else dic_params["show_plots"]

      rep_plots   = int(num_epoch/100) if "rep_plots" not in dic_params \
         else dic_params["rep_plots"]

      if num_dims == 1:
         keep_slices = True # since there is no slices anymore


      ##################################
      #
      #           Trainings
      #
      ##################################

      loss_func = torch.nn.MSELoss(reduction='mean')

      # Learning phase
      losses=[]
      true_q = torch.arange(1, num_samples+1, dtype=self._dtype, device=self._device) / num_samples
      qregressor.train()
      for epoch in range(num_epoch + 1):
         # 1. Slice related work
         if epoch == 0 or not keep_slices:
            # 1a. Generate slices
            tens_slices = torch.nn.functional.normalize(
               torch.randn(num_slices, num_dims, dtype=self._dtype, device=self._device), p=2, dim=1
            )
            tens_slices[:, num_dims-1] = torch.abs(tens_slices[:, num_dims-1])

            # 1b. Project data onto slices
            tens_proj = tens_slices @ data_object

            # 1c. Compute quantile function for each slice
            sorted_samples = torch.sort(tens_proj, dim=1)[0]

         # 2. compute estimated quantile
         x_pred = qregressor.forward(true_q, tens_slices)
         # torch.stack(
         #    [qregressor.forward(true_q, tens_slices[l, :].view(1, num_dims))
         #    for l in range(num_slices)], dim=0).squeeze(2)

         # 3. loss evaluation, 
         loss = loss_func(sorted_samples, x_pred)

         losses.append(loss.clone().detach().cpu().numpy())
         loss.backward() 
          
         optimizer.step()
         optimizer.zero_grad()

         if scheduler is not None:
            scheduler.step(loss)

         # 4. Eventualy plot stuff
         if show_plot and epoch%rep_plots==0:
            print("Iteration{}. Loss={:4e}".format(epoch, loss.clone().detach().cpu().numpy()))

            max_slices = 4

            plt.close()
            fig, ax = plt.subplots(1, min(num_slices, max_slices), figsize=(min(num_slices, max_slices)*4, 4))
            for i in range(min(num_slices, max_slices)):

               if min(num_slices, max_slices) == 1:
                  myax = ax
               else:
                  myax = ax[i]

               # x_pred = self.forward(true_q.unsqueeze(1), tens_slices_batch[i,:].view(1, num_dims).repeat(num_samples, 1))
               myax.plot(true_q.clone().cpu().numpy(), sorted_samples[i,:].clone().detach().cpu().numpy(),'.',c='r',label='True')
               myax.plot(true_q.clone().cpu().numpy(), x_pred[i, :].clone().detach().cpu().numpy(),'.',c='b',label='Predicted')
               myax.legend(loc='best')

            plt.tight_layout()
            plt.show(block=False)
            plt.pause(.1)

         # 5. Eventually save
         self.save_model(qregressor, file_name)

      qregressor.eval()
