from abc import ABC, abstractmethod

import torch


class AQRegressor(torch.nn.Module, ABC):
   """Abstract class trainer

      To instantiate it, just inplement the
      so called 
   """
   def __init__(self, dtype, device):
      super(AQRegressor, self).__init__()

      self.type(dtype).to(device=device)

      self._dtype  = dtype
      self._device = device


   @abstractmethod
   def forward(self, q, slices):
      """
      Parameters:
      -----------
      quantiles: (n_quantiles,)
         the quantiles to estimate
      slices: (n_slices, input_dim):
         the slices parmeters along which we want
         the quantiles. 

      Returns:
      --------
      (n_slices, n_quantiles) tensor
         desired quantiles
      """

   pass


   def train(self):
      for param in self.parameters():
         param.requires_grad = True


   def eval(self):
      for param in self.parameters():
         param.requires_grad = False


class DIY_Qregressor(AQRegressor):
   """
      Do it yourself Qregressor
      
      take a torch.nn.sequential as input
   """

   def __init__(self, sequential):
      super(DIY_Qregressor, self).__init__()
      
      # Assert stuff
      assert(sequential == torch.nn.Sequential)

      self.net = sequential

