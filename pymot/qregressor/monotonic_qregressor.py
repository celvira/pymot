from pymot.qregressor.aqregressor import AQRegressor
from pymot.qregressor._monotonic_stuff import *

import torch

class QRegressorMonotonic(AQRegressor):
   """QRegressor network, to predict quantiles from slices"""

   def __init__(self, input_dim, shapes=[256, 128, 64], nb_steps_integration=100, dtype=torch.float64, device=torch.device('cpu')):
      """
         Create a QRegressor network

         Parameters:
         -----------
         input_dim: int
            the slices dimension
         shapes: list of ints
            every entry gives a hidden size for the network.
      """
      super(QRegressorMonotonic, self).__init__(dtype, device)

      # initialize the monotonic network

      self.mononet = MonotonicNN(input_dim + 1, shapes, nb_steps=nb_steps_integration, dev=device).to(device)
            

   def forward(self, q, slices):
      """
      Parameters:
      -----------
      quantiles: (n_quantiles,)
          the quantiles to estimate
      slices: (n_slices, input_dim):
          the slices parmeters along which we want
          the quantiles. 

      Returns:
      --------
      (n_slices, n_quantiles) tensor
          desired quantiles
      """
      # get the dimensions
      q = q.flatten()
      n_quantiles = q.numel()
      n_slices, ambiant_dim = slices.shape

      # making q (n_slices, n_quantiles)
      q = q[None].repeat((n_slices, 1))
      # making slices (n_slices, n_quantiles, dimspace)
      slices = slices[:, None, :].repeat((1, n_quantiles, 1))

      # reshape all to have first dimension n_slices * n_quantiles
      q = q.view(-1, 1)
      slices = slices.view(-1, ambiant_dim)

      x = self.mononet(q,slices)
      return x.view(n_slices, n_quantiles)


   def train(self):
      for param in self.mononet.parameters():
         self.mononet.requires_grad = True


   def eval(self):
      for param in self.mononet.parameters():
         self.mononet.requires_grad = False


