from abc import ABC, abstractmethod

import torch


class ATrainer():
   """Abstract class trainer

      To instantiate it, just inplement the
      so called 
   """

   def __init__(self, dtype=torch.float64, device=torch.device('cpu')):

      super(ATrainer, self).__init__()
      self._dtype  = dtype
      self._device = device


   def train(self, data_object, qregressor, dict_params):
      """
      Parameters
      ----------
         data_object 
            contains data, type is trainer dependant
         qregressor : instance of AQregressor
            qregressor to be trained
         dict_params 
            dictionary containing trainer options

      """

      raise NotImplemented

   def save_model(self, qregressor, file_name=None):

      if file_name is not None:
         torch.save(qregressor.state_dict(), file_name)