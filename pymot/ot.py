import torch

from pymot.moments import Moments
from pymot.quantile import get_tens_quantile
from pymot.quantilefit import AQuantileRegressor

from pymot.utils import _compute_stairway_tensor
# from torchsearchsorted import searchsorted


def sliced_wasserstein(dataset_1, dataset_2, p, 
                     dtype=torch.float64, device=torch.device('cpu')):
   """ Pytorch implementation of the sliced Wasserstein distance.
   Take projected samples as input and return (Monte-Carlo) estimator
   of the the Sliced Wassertein distance.

   Parameters
   ----------
   dataset_1 : torch.Tensor (or pymot.utils.Moments, AQuantileRegressor)
      projected samples from distribution 1
      size : num_slices x num_samples
   dataset_2 : torch.Tensor (or pymot.utils.Moments)
      projected samples from distribution 2
      size : num_slices x num_samples
   p : float
      power related to the underlying distance
   dtype : torch.dtype
      Tensor type (default : torch.float64)
   device : torch.device
      torch.device('cpu')

   Return
   ------
   s_wass : torch.Tensor
      Wasserstein distance
      size 1
   """

   # Classical OT

   if isinstance(dataset_1, torch.Tensor) and isinstance(dataset_2, torch.Tensor):
      return _sliced_wasserstein(dataset_1, dataset_2, p, dtype, device)

   # Moment OT

   elif isinstance(dataset_1, torch.Tensor) and isinstance(dataset_2, Moments):
      return _mixed_sliced_wasserstein(dataset_1, dataset_2, p, dtype, device)

   elif isinstance(dataset_2, torch.Tensor) and isinstance(dataset_1, Moments):
      return _mixed_sliced_wasserstein(dataset_2, dataset_1, p, dtype, device)

   elif isinstance(dataset_1, Moments) and isinstance(dataset_2, Moments):
      return _moments_sliced_wasserstein(dataset_1, dataset_2, p, dtype, device)

   # Qfit stuff

   elif isinstance(dataset_1, torch.Tensor) and isinstance(dataset_2, AQuantileRegressor):
      return _mixed_qfit_sliced_wasserstein(dataset_1, dataset_2, p, dtype, device)

   elif isinstance(dataset_2, torch.Tensor) and isinstance(dataset_1, AQuantileRegressor):
      return _mixed_qfit_sliced_wasserstein(dataset_2, dataset_1, p, dtype, device)

   else:
      raise ValueError("dataset_1 and dataset_2 must be torch.tensor or Moments")


def _sliced_wasserstein(tens_proj_samples_1, tens_proj_samples_2, p, dtype, device):
   """ Pytorch implementation of the sliced Wasserstein distance.
   Take projected samples as input and return (Monte-Carlo) estimator
   of the the Sliced Wassertein distance.

   Parameters
   ----------
   tens_proj_samples_1 : torch.Tensor
      projected samples from distribution 1
      size : num_slices x num_samples
   tens_proj_samples_2 : torch.Tensor
      projected samples from distribution 2
      size : num_slices x num_samples
   p : float
      power related to the underlying distance
   dtype : torch.dtype
      Tensor type (default : torch.float64)
   device : torch.device
      torch.device('cpu')

   Return
   ------
   s_wass : torch.Tensor
      Wasserstein distance
      size 1
   """

   assert(len(tens_proj_samples_1.size()) == 2)
   assert(len(tens_proj_samples_2.size()) == 2)

   ##################################################
   # Compute the sliced wasserstein distance by   #
   # sorting the samples per random projection and  #
   # calculating the difference between the         #
   # encoded samples and drawn random samples       #
   # per random projection                          #
   ##################################################

   # 1. Difference of (sorted samples)
   tens_diff_samples = (torch.sort(tens_proj_samples_1, dim=1)[0] -
      torch.sort(tens_proj_samples_2, dim=1)[0])

   # 2. distance between latent space prior and encoded distributions
   tens_diff_samples = torch.pow(tens_diff_samples, p)
 
   # 3. approximate mean wasserstein_distance for each projection
   return tens_diff_samples.mean()


def _mixed_sliced_wasserstein(
      tens_proj_samples_1, moments_2,
      p, dtype=torch.float64, device=torch.device('cpu')):
   """ Compute the Sliced-Wasserstein distance between a set of points
   and a distribution known only trough its first ordinary moments

   Parameters
   ----------
   tens_proj_samples_1 : toch.Tensor
      tensor of samples from distribution 1
      size num_slices x num_samples
   moments_2 : pymot.om.moments.OMMoments
      Collection of moments
   p : float
      power of distance metric
   dtype : torch.dtype
      Tensor type (default : torch.float64)
   device : torch.device
      torch device (default 'cpu')
   
   Returns
   -------
   ms_wass : torch.Tensor
      Approximated Sliced Wasserstrain distances 
      size 1
   """

   # 0. Parameters
   assert(isinstance(moments_2, Moments))

   if len(tens_proj_samples_1.size()) == 1:
      num_slices = 1
      num_samples = tens_proj_samples_1.size(0)

   elif len(tens_proj_samples_1.size()) == 2:
      num_slices = tens_proj_samples_1.size(0)
      num_samples = tens_proj_samples_1.size(1)
   
   else:
      ValueError("tens_proj_samples_1 must be a (num_slices x num_samples) tensor")
   
   num_moments = moments_2.num_moments
   assert(num_slices == moments_2.num_slices)

   # 1a. Compute quantile function distribution 1
      # size : num_slices x num_samples
   tens_qfunc_1 = torch.sort(tens_proj_samples_1, dim=1)[0]

   # 1b. Compute quantile function estimators distribution 2
      # size : num_slices x num_moments
   tens_qfunc_2 = get_tens_quantile(moments_2, dtype, device)


   # 2. Of wizardry and science
   #   make tens_qfunc of the same size
   TENS_STAIRWAY = \
      _compute_stairway_tensor(num_moments+1, num_samples, dtype=dtype, device=device)


   # 4. Compute Slice Wasserstein distance
   ms_wass = tens_qfunc_1 - tens_qfunc_2 @ TENS_STAIRWAY


   # 5. distance between latent space prior and encoded distributions
   ms_wass = torch.pow(ms_wass, p)


   # 6. approximate mean wasserstein_distance for each projection
   return ms_wass.mean()


def _moments_sliced_wasserstein(
      moments_1, moments_2, p,
      dtype=torch.float64, device=torch.device('cpu')):
   """ (Approximation of) Sliced Wasserstein distance from moments

      Parameters
      ----------
      moments_1 : pymot.moments.Moments
         Collection of moments
      moments_2 : pymot.moments.Moments
         Collection of moments
      p : float
         power of distance metric
      dtype : torch.dtype
         Tensor type (default : torch.float64)
      device : torch.device
         torch device (default 'cpu')

      Return
      ------
      ms_wass : torch.Tensor
         Approximated Sliced Wasserstrain distances 
         size 1
   """

   # 0. checking
   assert(isinstance(moments_1, Moments))
   assert(isinstance(moments_2, Moments))

   assert(moments_1.num_slices == moments_2.num_slices)

   if moments_1.num_moments != moments_2.num_moments:
      raise NotImplementedError

   # 1. Parameters
   num_slices = moments_1.num_slices
   num_moments = moments_1.num_moments

   # 2. Compute quantile function estimators
   tens_qfunc_1 = get_tens_quantile(moments_1, dtype, device)
   tens_qfunc_2 = get_tens_quantile(moments_2, dtype, device)

   # 3. Compute Slice Wasserstein distance
   ms_wass = tens_qfunc_1 - tens_qfunc_2

   # 4. distance between latent space prior and encoded distributions
   ms_wass = torch.pow(ms_wass, p)

   # 5. approximate mean wasserstein_distance for each projection
   return ms_wass.mean()


def _mixed_qfit_sliced_wasserstein(
      tens_proj_samples_1, quantileRegressor_2,
      p, dtype=torch.float64, device=torch.device('cpu')):
   """ Compute the Sliced-Wasserstein distance between a set of points
   and a distribution known only trough its first ordinary moments

   Parameters
   ----------
   tens_proj_samples_1 : toch.Tensor
      tensor of samples from distribution 1
      size num_slices x num_samples
   moments_2 : pymot.om.moments.OMMoments
      Collection of moments
   p : float
      power of distance metric
   dtype : torch.dtype
      Tensor type (default : torch.float64)
   device : torch.device
      torch device (default 'cpu')
   
   Returns
   -------
   ms_wass : torch.Tensor
      Approximated Sliced Wasserstrain distances 
      size 1
   """

   # 0. Parameters
   assert(isinstance(quantileRegressor_2, AQuantileRegressor))

   if len(tens_proj_samples_1.size()) == 1:
      num_slices = 1
      num_samples = tens_proj_samples_1.size(0)

   elif len(tens_proj_samples_1.size()) == 2:
      num_slices = tens_proj_samples_1.size(0)
      num_samples = tens_proj_samples_1.size(1)
   
   else:
      ValueError("tens_proj_samples_1 must be a (num_slices x num_samples) tensor")
   
   assert(num_slices == 1)

   # 1a. Compute quantile function distribution 1
      # size : num_slices x num_samples
   tens_qfunc_1 = torch.sort(tens_proj_samples_1, dim=1)[0]

   # 1b. Compute quantile function estimators distribution 2
      # size : num_slices x num_moments
   true_q = torch.arange(1, num_samples+1, dtype=dtype, device=device) / num_samples
   tens_qfunc_2 = quantileRegressor_2.forward(true_q.unsqueeze(1), true_q.unsqueeze(1)).T


   # 4. Compute Slice Wasserstein distance
   ms_wass = tens_qfunc_1 - tens_qfunc_2


   # 5. distance between latent space prior and encoded distributions
   ms_wass = torch.pow(ms_wass, p)

   # 6. approximate mean wasserstein_distance for each projection
   return ms_wass.mean()