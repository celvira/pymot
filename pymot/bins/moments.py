import torch

from pymot.moments import Moments

class BinsMoments(Moments):
   """ Compute bins corresponding to a stairway approximation
   of the quantile function

   Parameters
   ----------
   tens_moments : torch.Tensor
      matrice of moments
      size num_slices x num_moments
   num_moments : int
      total number of moment

   Attributes
   ----------
   tens_moments : torch.Tensor
      size num_slices x num_moments
   num_slices : int
      number of slices
   num_moments : int
      total number of moment
   str_moment_name : str
      name of the family of moments
   """
   def __init__(self, tens_moments, 
                dtype=torch.float64, device=torch.device('cpu')):

      super(BinsMoments, self).__init__(
         tens_moments,
         None,
         None,
         dtype,
         device)

      self.str_moment_name = 'bins'


   def __getitem__(self, key):
      return FellerMoments(
         self.BinsMoments[key, :],
         self.dtype,
         self.device
      )


def _estimate_bins_moments(tens_project_samples, num_moments,
   dtype=torch.float64, device=torch.device('cpu')):
   """ Compute ordinary moments ranging from 0 to num_moments-1.
   
   In particular, compute
      \\int_0^{+\\infty} (x - \\min(x_s)**l dx
   for each slice s and l ranging from 0 to num_moments-1 

   Parameters
   ----------
   tens_project_samples : torch.Tensor
      projected sampels
      num-slices x num_samples
   num_moments : float
      number of desired moments
   dtype : torch.dtype
      Tensor type (default: torch.float64)
   device : torch.device
      Desired device (default: cpu)

   Returns
   -------
   tens_moments : torch.Tensor
      size num_slices x num_moments
   tens_min : torch.Tensor
      size num_slices
   tens_length : torch.Tensor
      size num_slices
   """

   num_slices  = tens_project_samples.size(0)
   num_samples = tens_project_samples.size(1)

   #Create magic bin tensor
   # TODO : version simplifiée, pb aux frontières
   list_entries = []
   list_mass    = []
   cur_n = 0
   q_emp = 0
   for l in range(num_moments):
      sub_list = []

      while float(cur_n) / float(num_samples) <= (l + 1.) / float(num_moments):

         sub_list.append(int(cur_n))
         cur_n += 1

         if cur_n == num_samples:
            break

      for i in sub_list:
         list_entries.append([i, l])
         list_mass.append(1. / float(len(sub_list)))


   i = torch.LongTensor(list_entries, device=device)
   v = torch.DoubleTensor(list_mass, device=device)
   tens_magic = torch.sparse.DoubleTensor(i.t(), v, torch.Size([num_samples, num_moments]))


   # 2. Moments
   # tens_moments = torch.zeros(num_slices, num_moments)
   tens_moments = torch.sort(tens_project_samples, dim=1)[0] @ tens_magic.to_dense()

   # 3. Output
   return BinsMoments(tens_moments)


if __name__ == '__main__':
   # Parameters
   num_slices = 10
   num_samples = 50
   num_moments = 5

   # Data
   tens_A = torch.randn([num_slices, num_samples], dtype=torch.float64)

   # Call function
   _estimate_bins_moments(tens_A, num_moments)
