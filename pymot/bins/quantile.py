import torch, math

from pymot.bins.moments import BinsMoments


def _get_func_quantile(moments, dtype=torch.float64, device=torch.device('cpu')):
   """ Compute approximation of quantile function base on Bins moments

    Implements [1, Eq.~(28)] but using Bins moment instead.

    [1] R. M. Mnatsakanov and A. Sborshchikovi, "Recovery of a quantile function from moments",
    Journal of Computational and Applied Mathematics  315  354 - 364  (2017) using Bins cdf estimator

   Parameters
   ----------
   moments : pymot.bins.moments.BinsMoments
      Collection of moments
   dtype : torch.dtype
      Tensor type
      Default torch.float64
   device : torch.device
      where to perform calculations

   Returns
   -------
   qfunc : lambda function 
      tensor of quantile function (one line per projection)
      size num_slices x num_moments+1
   """

   # 0. Tensor dtype
   # 0. Parameters learning / checking
   assert(isinstance(moments, BinsMoments))

   if moments.num_slices > 1:
      raise NotImplemented


   # 3. Quantile function
   def estimator(q):

      assert(q >= 0 and q <= 1)

      if q < 1:
         floor = int(math.floor(moments.num_moments * q))
      else:
         floor = moments.num_moments - 1

      return moments.tens_moments[0, floor]

   return estimator


def _get_tens_quantile(moments, dtype=torch.float64, device=torch.device('cpu')):
   """ Compute approximation of quantile function base on Bins moments

    Implements [1, Eq.~(28)] but using Bins moment instead.

    [1] R. M. Mnatsakanov and A. Sborshchikovi, "Recovery of a quantile function from moments",
    Journal of Computational and Applied Mathematics  315  354 - 364  (2017) using Bins cdf estimator

   Parameters
   ----------
   moments : pymot.feller.moments.BinsMoments
      Collection of moments
   dtype : torch.dtype
      Tensor type
      Default torch.float64
   device : torch.device
      where to perform calculations

   Returns
   -------
   qfunc : lambda function 
      tensor of quantile function (one line per projection)
      size num_slices x num_moments+1
   """

   # 0. Checking and parameters
   assert(isinstance(moments, BinsMoments))

   return moments.tens_moments.clone()
