import torch


class Moments(object):
   """ Abstract class moments

   Parameters
   ----------
   tens_moments : torch.Tensor
      matrice of moments
      size num_slices x num_moments
   tens_min : torch.Tensor
      size num_slices
   tens_length : torch.Tensor
      size num_slices

   Attributes
   ----------
   tens_moments : torch.Tensor
      size num_slices x num_moments
   tens_min : torch.Tensor
      size num_slices
   tens_length : torch.Tensor
      size num_slices
   num_slices : int
      number of slices
   num_moments : int
      total number of moment
   str_moment_name : str
      name of the family of moments
   """

   def __init__(self, tens_moments, tens_min, tens_length, dtype, device):
      super(Moments, self).__init__()

      if len(tens_moments.size()) == 1:
         self.num_slices  = 1
         self.num_moments = tens_moments.size(0) - 1

      elif len(tens_moments.size()) == 2:
         self.num_slices  = tens_moments.size(0)
         self.num_moments = tens_moments.size(1) - 1

      else:
         raise ValueError("tens_moments must a (num_slices, num_moments) Tensor")

      self.tens_moments = tens_moments.view(self.num_slices, self.num_moments+1)
      
      if tens_min is not None:
         self.tens_min    = tens_min.view(self.num_slices)

      if tens_length is not None:
         self.tens_length = tens_length.view(self.num_slices)

      self.str_moment_name = "None"

      self.dtype=dtype
      self.device=device


def estimate_moments(tens_project_samples, num_moments, type_moment,
   tens_min=None, tens_length=None,
   dtype=torch.float64, device=torch.device('cpu')):
   """ Compute the :math:`k` first Laplace moments associated to a set of samples
    
   .. math::
        m_\\ell = \\mathbb{E}_X\\left[ e^{-\\ell\\log(b)X} \\right]

   where :math:`X` is the uniform distribution on :math:`\\mathbb{R}_+`.

   Parameters
   ----------
   tens_project_samples : torch.Tensor
      projected sampels
      num-slices x num_samples
   num_moments : float
      number of desired moments
   type_moment : str
      moments to be computed
   tens_min : torch.Tensor
      size num_slices
   tens_length : torch.Tensor
      size num_slices
   dtype : torch.dtype
      Tensor type (default: torch.float64)
   device : torch.device
      Desired device (default: cpu)

   Returns
   -------
   tens_moments : torch.Tensor
      size num_slices x num_moments
   tens_min : torch.Tensor
      size num_slices
   tens_length : torch.Tensor
      size num_slices
   """

   if len(tens_project_samples.size()) == 1:
      num_slices  = 1
      num_samples = tens_project_samples.size(0)

   elif len(tens_project_samples.size()) == 2:
      num_slices  = tens_project_samples.size(0)
      num_samples = tens_project_samples.size(1)
   else:
      raise ValueError("tens_project_samples must a (num_slices, num_samples) Tensor")


   # 1. Min and length
   if tens_min is None:
      tens_min = torch.min(tens_project_samples.view(num_slices, num_samples), dim=1, keepdim=False)[0].view(num_slices).to(device)
   
   if tens_length is None:
      tens_length = torch.max(tens_project_samples.view(num_slices, num_samples), dim=1, keepdim=False)[0].view(num_slices).to(device) - tens_min

   # 2. moments
   if type_moment == 'om':
      from pymot.om.moments import _estimate_om_moments

      return _estimate_om_moments(tens_project_samples.view(num_slices, num_samples), num_moments, tens_min, tens_length, dtype, device)

   elif type_moment == 'laplace':
      raise NotImplementedError

   elif type_moment == 'feller':
      from pymot.feller.moments import _estimate_feller_moments

      return _estimate_feller_moments(tens_project_samples.view(num_slices, num_samples), num_moments, tens_min, tens_length, dtype, device)

   elif type_moment == 'bins':
      from pymot.bins.moments import _estimate_bins_moments

      return _estimate_bins_moments(tens_project_samples.view(num_slices, num_samples), num_moments, dtype, device)

   else:
      list_types = ['om', 'laplace', 'feller', 'bin']
      raise ValueError("type_moment should belong to " + str(list_types))

