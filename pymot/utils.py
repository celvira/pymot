import torch, math


TENS_CDF = torch.zeros(0, 0)
TENS_LOG_CDF = torch.zeros(0, 0)
TENS_SIGN_LOG_CDF = torch.zeros(0, 0)
TENS_STAIRWAY = torch.zeros(0, 0)

TENS_FELLER_CDF = torch.zeros(0)
TENS_FELLER_CDF_SIGN = torch.zeros(0)

TENS_LOG_BERSTEIN = torch.zeros(0)


def _computeLogBernsteinCoeff(num_moments, dtype, device):
   """ Compute a vector where each entry contains

   vec[i] = log(binom(num_moments, i))

   Inputs and outputs shoud be torch.float64

   Parameters
   ----------
   num_moments : int
      number of moments
   dtype : torch.dtype
      Tensor type
   device : torch.device
      where to perform computations

   Returns
   -------
   tens_F : torch.tensor
      so called intermediary cdf matrix
      size num_moments+1 x num_moments+1
   """

   # 1. design matrices for computation

   global TENS_LOG_BERSTEIN
   
   if TENS_LOG_BERSTEIN.size() == torch.Size([num_moments+1]):

      return TENS_LOG_BERSTEIN

   else:
      TENS_LOG_BERSTEIN = torch.zeros([num_moments+1], 
         dtype=dtype, device=device)

      let_lgamma_k = torch.lgamma(torch.tensor([num_moments + 1.], dtype=dtype, device=device))
      for l in range(num_moments + 1):
         TENS_LOG_BERSTEIN[l] = let_lgamma_k \
            - torch.lgamma(torch.tensor([l + 1.], dtype=dtype, device=device)) \
            - torch.lgamma(torch.tensor([num_moments - l + 1.], dtype=dtype, device=device))

      return TENS_LOG_BERSTEIN



def _computeCDFmat(num_moments, dtype, device):
   """ Compute a matrix that is usefull in the computation of 
   CDF approximation with moments

   Inputs and outputs are torch.float64

   Parameters
   ----------
   num_moments : int
      number of moments
   dtype : torch.dtype
      Tensor type
   device : torch.device
      where to perform computations

   Returns
   -------
   tens_F : torch.tensor
      so called intermediary cdf matrix
      size num_moments+1 x num_moments+1
   """

   # 1. design matrices for computation

   global TENS_CDF
   
   if TENS_CDF.size() == torch.Size((num_moments+1, num_moments+1)):

      return TENS_CDF

   else:
      TENS_CDF = torch.zeros([num_moments+1, num_moments+1], 
         dtype=dtype, device=device)

      for i in range(num_moments+1):
         let_gamln1 = torch.lgamma(torch.tensor(num_moments + 1., dtype=dtype)) - torch.lgamma(torch.tensor(i + 1., dtype=dtype))

         for j in range(i, num_moments+1):
            TENS_CDF[i, j] = torch.exp( \
              let_gamln1 - torch.lgamma(torch.tensor(num_moments - j + 1., dtype=dtype)) \
              - torch.lgamma(torch.tensor(j - i + 1., dtype=dtype)) \
            ) * (-1)**(j-i)

      return TENS_CDF


def _computeLogCDFmat(num_moments, dtype, device):
   """ Compute a matrix that is usefull in the computation of 
   CDF approximation with moments

   Inputs and outputs shoud be torch.float64

   Parameters
   ----------
   num_moments : int
      number of moments
   dtype : torch.dtype
      Tensor type
   device : torch.device
      where to perform computations

   Returns
   -------
   tens_F : torch.tensor
      so called intermediary cdf matrix
      size num_moments+1 x num_moments+1
   """

   # 1. design matrices for computation

   global TENS_LOG_CDF
   global TENS_SIGN_LOG_CDF
   
   if TENS_LOG_CDF.size() == torch.Size((num_moments+1, num_moments+1)):

      return TENS_LOG_CDF, TENS_SIGN_LOG_CDF

   else:
      TENS_LOG_CDF = torch.zeros([num_moments+1, num_moments+1], 
         dtype=dtype, device=device)
      TENS_SIGN_LOG_CDF = torch.zeros([num_moments+1, num_moments+1], 
         dtype=dtype, device=device)

      for i in range(num_moments+1):
         let_gamln1 = torch.lgamma(torch.tensor(num_moments + 1., dtype=dtype, device=device)) - torch.lgamma(torch.tensor(i + 1., dtype=dtype, device=device))

         for j in range(i, num_moments+1):
            TENS_LOG_CDF[i, j] = let_gamln1 \
              - torch.lgamma(torch.tensor(num_moments - j + 1., dtype=dtype, device=device)) \
              - torch.lgamma(torch.tensor(j - i + 1., dtype=dtype, device=device))

            TENS_SIGN_LOG_CDF[i, j] = -1. if (j-i) % 2 == 1 else 1.

      return TENS_LOG_CDF, TENS_SIGN_LOG_CDF


def _computeLogFellerVec(num_moments, dtype, device):
   """ Compute a matrix that is usefull in the computation of 
   CDF approximation with moments

   Inputs and outputs shoud be torch.float64

   Parameters
   ----------
   num_moments : int
      number of moments
   dtype : torch.dtype
      Tensor type
   device : torch.device
      where to perform computations

   Returns
   -------
   tens_F : torch.tensor
      so called intermediary cdf matrix
      size num_moments+1 x num_moments+1
   """

   # 1. design matrices for computation

   global TENS_FELLER_CDF
   
   if TENS_FELLER_CDF.size() == torch.Size([num_moments+1]):

      return TENS_FELLER_CDF

   else:
      TENS_FELLER_CDF = torch.zeros([num_moments+1], 
         dtype=dtype, device=device)

      for l in range(num_moments+1):
         TENS_FELLER_CDF[l] = torch.lgamma(torch.tensor(l + 1., dtype=dtype, device=device))

      return TENS_FELLER_CDF


def _compute_stairway_tensor(num_moments, num_samples, dtype, device):
   """
   """

   global TENS_STAIRWAY

   if TENS_STAIRWAY.size() == torch.Size([num_moments, num_samples]):
      # Save computation time...
      # TODO check also dtype and device....

      return TENS_STAIRWAY

   else:
      TENS_STAIRWAY = torch.zeros(num_moments, num_samples, 
         dtype=dtype, device=device)
      for n in range(num_samples):
         q = float(n) / float(num_samples)
         floor = int(math.floor(num_moments * q))
         TENS_STAIRWAY[floor, n] = 1.

      return TENS_STAIRWAY
