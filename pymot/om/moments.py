import torch

from pymot.moments import Moments


class OMMoments(Moments):
   """ Class containing Ordinary moments
   ..math::
      m_\\ell = \\mathbb{E}_X\\left[ e^{-\\lambda X} X^\\ell \\right]
   for \\ell ranging from 0 to num_moments - 1

   Parameters
   ----------
   tens_moments : torch.Tensor
      matrice of moments
      size num_slices x num_moments
   tens_min : torch.Tensor
      size num_slices
   tens_length : torch.Tensor
      size num_slices

   Attributes
   ----------
   tens_moments : torch.Tensor
      size num_slices x num_moments
   tens_min : torch.Tensor
      size num_slices
   tens_length : torch.Tensor
      size num_slices
   num_slices : int
      number of slices
   num_moments : int
      total number of moment
   str_moment_name : str
      name of the family of moments
   """
   def __init__(self, tens_moments, tens_min, tens_length, tens_lbd,
                dtype=torch.float64, device=torch.device('cpu')):

      super(OMMoments, self).__init__(
         tens_moments,
         tens_min,
         tens_length,
         dtype,
         device)

      self.str_moment_name = 'om'


   def __getitem__(self, key):
      return OMMoments(
         self.tens_moments[key, :],
         self.tens_min[key],
         self.tens_lbd[key],
         self.dtype,
         self.device
      )


def _estimate_om_moments(tens_project_samples, num_moments,
   tens_min, tens_length,
   dtype=torch.float64, device=torch.device('cpu')):
   """ Compute ordinary moments ranging from 0 to num_moments-1.
   
   In particular, compute
      \\int_0^{+\\infty} (x - \\min(x_s)**l dx
   for each slice s and l ranging from 0 to num_moments-1 

   Parameters
   ----------
   tens_project_samples : torch.Tensor
      projected sampels
      num-slices x num_samples
   num_moments : float
      number of desired moments
   tens_min : torch.Tensor
      size num_slices
   tens_length : torch.Tensor
      size num_slices
   dtype : torch.dtype
      Tensor type (default: torch.float64)
   device : torch.device
      Desired device (default: cpu)

   Returns
   -------
   moments : pymot.om.moments.OMMoments
   """

   num_slices  = tens_project_samples.size(0)
   num_samples = tens_project_samples.size(1)

   # 1. Moments
   # tens_moments = torch.zeros(num_slices, num_moments)
   tens_moments = torch.stack([
         torch.mean((tens_project_samples - tens_min.view(num_slices, 1).repeat(1, num_samples)).pow(l), dim=1) for l in range(num_moments)
      ]).T

   # 2. Output
   return OMMoments(tens_moments, tens_min, tens_length, dtype, device)


def beta_moments(k, a, b, dtype=torch.float64, device=torch.device('cpu')):
   """ Compute (in closed form) the $k$ first moments of the beta distribution
   with parameters a,b
   """

   tens_moments = torch.zeros([k], dtype=dtype, device=device)
   tens_moments[0] = 1.
   for l in range(1, k):
      tens_moments[l] = tens_moments[l - 1] * (a + l - 1.) / (a + b + l - 1.)

   return OMMoments(tens_moments, 
                    torch.zeros([1], dtype=dtype, device=device),
                    torch.ones([1], dtype=dtype, device=device),
                    dtype,
                    device
                    )


def unif_moments(k, T, dtype=torch.float64, device=torch.device('cpu')):
   """
   k: number of desired moments
   """

   if type(T) != torch.Tensor:
      T = torch.tensor(T, dtype=dtype, device=device)
   else:
      assert(T.dtype == dtype)

   if T.device != device:
      T.to(device)
   
   tens_moments = torch.zeros([k], dtype=dtype, device=device)
   tens_moments[0] = 1
   for l in range(1, k):
      tens_moments[l] = torch.pow(T, l) / torch.tensor(l+1, dtype=dtype, device=device)

   return OMMoments(tens_moments, 
                    torch.zeros([1], dtype=dtype, device=device),
                    T * torch.ones([1], dtype=dtype, device=device),
                    dtype,
                    device
                    )


if __name__ == '__main__':
   # Parameters
   num_slices = 10
   num_samples = 20
   num_moments = 5

   # Data
   tens_A = torch.randn([num_slices, num_samples], dtype=torch.float64)

   # Call function
   compute_ordinary_moments(tens_A, num_moments)
