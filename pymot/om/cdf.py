import torch, math

from pymot.utils import _computeCDFmat
from pymot.om.moments import OMMoments


def get_cdf(moments, dtype=torch.float64, device=torch.device('cpu')):
   """ Compute approximation of quantile function base on ordinary moments

   Parameters and outputs are dtype=torch.float64

   Parameters
   ----------
   moments : pymot.om.moments.OMMoments
      Collection of moments
   dtype : torch.dtype
      Tensor type
      Default torch.float64
   device : torch.device
      where to perform calculations

   Returns
   -------
   cdf : lambda function 
      tensor of quantile function (one line per projection)
      size num_slices x num_moments+1
   """

   # 0. Tensor dtype
   assert(isinstance(moments, OMMoments))

   if moments.num_slices > 1:
      raise NotImplemented


   tens_cdf = _get_tens_cdf(moments, dtype, device)

   # 3. Define cdf estimator
   def estimator(x):
      x_norm = (x - moments.tens_min.item()) / moments.tens_length.item()

      assert(x_norm >= 0 and x_norm <= 1.)
      floor = int(math.floor(moments.num_moments * x_norm))

      return tens_cdf[0, floor]

   return estimator


def _get_tens_cdf(moments, dtype=torch.float64, device=torch.device('cpu')):
   """ Compute approximation of quantile function base on ordinary moments

   Parameters and outputs are dtype=torch.float64

   Parameters
   ----------
   moments : pymot.om.moments.OMMoments
      Collection of moments
   dtype : torch.dtype
      Tensor type
      Default torch.float64
   device : torch.device
      where to perform calculations

   Returns
   -------
   cdf : lambda function 
      tensor of quantile function (one line per projection)
      size num_slices x num_moments+1
   """

   # 0. Tensor dtype
   assert(isinstance(moments, OMMoments))

   num_moments = moments.num_moments
   num_slices  = moments.num_slices

   # 1. Compute CDF mat
      # num_slices x num_moments+1 tensor
   tens_buf_cdf  = _computeCDFmat(num_moments, dtype, device)

   tens_exponents = torch.arange(0., num_moments + 1., dtype=dtype, device=device)
   tens_cdf = torch.stack([ tens_buf_cdf 
      @ torch.diag(1. / moments.tens_length[d].pow(tens_exponents))
      @ moments.tens_moments[d, :]
      for d in range(num_slices)], dim=0)

      # num_slices x num_moments+1 tensor
   tens_cdf = torch.cumsum(tens_cdf, dim=1)

   # Finally
   return tens_cdf

