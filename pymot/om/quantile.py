import torch, math

#torch.set_default_tensor_type(torch.DoubleTensor)
from pymot.om.moments import OMMoments
from pymot.om.cdf import _get_tens_cdf
from pymot.utils import _computeCDFmat, _computeLogBernsteinCoeff


def _get_func_quantile(moments, dtype=torch.float64, device=torch.device('cpu')):
   """ Compute approximation of quantile function base on ordinary moments

   Parameters and outputs are dtype=torch.float64

   Parameters
   ----------
   moments : pymot.om.moments.OMMoments
      Collection of moments
   dtype : torch.dtype
      Tensor type
      Default torch.float64
   device : torch.device
      where to perform calculations

   Returns
   -------
   qfunc : lambda function 
      tensor of quantile function (one line per projection)
      size num_slices x num_moments+1
   """

   # 0. Tensor dtype
   assert(isinstance(moments, OMMoments))

   if moments.num_slices > 1:
      raise NotImplemented

   tensor_quantile = _get_tens_quantile(moments, dtype, device)

   # 3. Define cdf estimator
   def estimator(q):

      if q >= 1:
         return moments.tens_min.item() + moments.tens_length.item()

      assert(q >= 0 and q < 1.)
      floor = int(math.floor(moments.num_moments * q))

      return tensor_quantile.squeeze()[floor]

   return estimator


def _get_tens_quantile(moments, dtype=torch.float64, device=torch.device('cpu')):
   """ Compute approximation of quantile function base on ordinary moments

   Parameters and outputs are dtype=torch.float64

   Parameters
   ----------
   moments : pymot.om.moments.OMMoments
      Collection of moments
   dtype : torch.dtype
      Tensor type
      Default torch.float64
   device : torch.device
      where to perform calculations

   Returns
   -------
   tens_quantile : torch.Tensor 
      tensor of quantile function (one line per projection)
      size num_slices x num_moments+1
   """

   # 0. Tensor dtype
   assert(isinstance(moments, OMMoments))

   # 0bis. Get default parameters
   num_slices  = moments.num_slices
   num_moments = moments.num_moments

   # 1. Compute CDF mat
      # num_slices x num_moments+1 tensor
   tens_cdf = _get_tens_cdf(moments, dtype, device)


   # 2. Compute QF mat
   EPS = 10**(-4)
   vec_log_Bernstein = _computeLogBernsteinCoeff(num_moments, dtype, device)

   buf_quantile = torch.stack([ \
      torch.sum(torch.exp(vec_log_Bernstein[i] + i * (tens_cdf + EPS).log() 
          + (num_moments-i) * (1. - tens_cdf + EPS).log()), dim=1) \
          for i in range(num_moments+1)
      ], dim=1)

   tensor_quantile = torch.diag(moments.tens_length) @ torch.cumsum(buf_quantile, dim=1) \
      / torch.tensor(num_moments, dtype=dtype, device=device)
   tensor_quantile += moments.tens_min.view(num_slices, 1).expand(num_slices, num_moments+1)

   return tensor_quantile

